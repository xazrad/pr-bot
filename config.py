import os
from importlib import import_module

from celery.schedules import crontab
from pathlib import Path


class ConfigMeta(type):
    def __new__(cls, name, bases, attrs):
        # переопределим из переменной окружения
        for k in (r for r in override_attrs.keys() if not r.startswith('__') and r in os.environ):
            attrs[k] = os.environ[k]
        return super().__new__(cls, name, bases, override_attrs)


class Config(metaclass=ConfigMeta):
    DEBUG = False
    SECRET_KEY = ''
    HETZNER_SECRET = ''
    HETZNER_SSH_KEYS = [68337, 68338, 68353, 72534]
    SCALEWAY_AUTH_TOKEN = ''
    SCALEWAY_ORGANIZATION = ''
    DO_ACCESS_TOKEN = ''
    VULT_ACCESS_TOKEN = ''
    FREE_TEST_DRIVE_MINS = 60
    OVERDUE_BEFORE_STOP_SERVICE_DAYS = 5
    BOT_TOKEN = ''
    BOT_NAME = 'setproxy_bot'
    MAX_DOCKER_CONTAINER_SERVER = 5
    PORT_SERVER = [443, 1443, 2443, 3443, 4443]

    CLOUDFLARE_EMAIL = 'xazrad@gmail.com'
    CLOUDFLARE_TOKEN = ''

    ROUTE53_ZONE_ID = ''
    ROUTE53_DOMAIN = 'xazrad.me'

    MAILGUN_URL = ''
    MAILGUN_KEY = ''
    # TODO: добавить POLL SIZE
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    BASE_DIR = Path(__file__).parent
    KEYS_DIR = Path(Path(__file__).parent.parent, 'keys')

    PAYPAL_VERIFY_URL_PROD = 'https://ipnpb.paypal.com/cgi-bin/webscr'
    PAYPAL_VERIFY_URL_TEST = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr'

    COINGATE_ENV_PROD = 'https://api.coingate.com/v2'
    COINGATE_ENV_TEST = 'https://api-sandbox.coingate.com/v2'
    COINGATE_SECRET_TEST = ''
    COINGATE_SECRET_PROD = ''

    AMPLITUDE_API_KEY = ''

    BABEL_TRANSLATION_DIRECTORIES = ';'.join([
        os.path.join(str(BASE_DIR), 'app', 'translations'),
        os.path.join(str(BASE_DIR), 'celery_app', 'translations')
    ])
    BABEL_DEFAULT_LOCALE = 'ru'

    CELERY_IMPORTS = ('celery_app.tasks',)
    CELERY_TIMEZONE = 'UTC'
    CELERY_TASK_ROUTES = {
        'celery_app.tasks.check_service_agent': {'queue': 'agent'},
        'celery_app.tasks.check_server_agent': {'queue': 'agent'},
    }
    CELERY_ROUTES = CELERY_TASK_ROUTES

    CELERYBEAT_SCHEDULE = {
        # 'debug_periodic_task': {'task': 'celery_app.tasks.debug_periodic_task',
        #                         'schedule': timedelta(minutes=1),
        #                         'args': ()},
        'reminder_of_payment': {'task': 'celery_app.tasks.reminder_of_payment',
                                'schedule': crontab(hour=9, minute=30),
                                'args': ()},
        'removing_unused_servers': {'task': 'celery_app.tasks.removing_unused_servers',
                                    'schedule': crontab(hour=23, minute=30),
                                    'args': ()},  # происходит ранее removing_unpaid_services
                                                  # чтобы сервер жил еще около суток
        'stop_unpaid_services': {'task': 'celery_app.tasks.stop_unpaid_services',
                                     'schedule': crontab(hour=0, minute=10),
                                     'args': ()},
        'check_blocked_servers': {'task': 'celery_app.tasks.check_blocked_servers',
                                  'schedule': crontab(minute='*/10'),
                                  'args': ()},
    }


class Production(Config):
    DEBUG = False
    HETZNER_SECRET = ''
    HETZNER_SSH_KEYS = [74345, 74347, 74348, 74349]
    FREE_TEST_DRIVE_MINS = 60
    BOT_TOKEN = ''
    BOT_NAME = 'OwnProxyBot'

    SERVER_HOST = 'tg.mtproxy.shop'
    PREFERRED_URL_SCHEME = 'https'

    SQLALCHEMY_DATABASE_URI = 'postgresql://mtps_user:*@127.0.0.1:5432/mtproxyshop'
    REDIS_URL = "redis://:@localhost:6379/0"
    CELERY_RESULT_BACKEND = REDIS_URL
    BROKER_URL = 'amqp://q_app:vjsd9012s@localhost:5672/vhost2'

    AMPLITUDE_API_KEY = ''

    ROUTE53_ZONE_ID = ''
    ROUTE53_DOMAIN = 'likesky.blue'


class Develop(Config):
    SERVER_HOST = '127.0.0.1:8080'
    PREFERRED_URL_SCHEME = 'https'
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:idf1ah8s89@127.0.0.1:9452/mtproxyshop'
    REDIS_URL = "redis://:@localhost:6379/0"
    CELERY_RESULT_BACKEND = REDIS_URL
    BROKER_URL = 'amqp://q_app:vjsd9012s@localhost:5672/vhost2'


class Staging(Develop):
    SERVER_HOST = 'mtp-bot.ndees.cloud'
    PREFERRED_URL_SCHEME = 'https'
    DEBUG = False
    REDIS_URL = "redis://:@localhost:6379/0"
    CELERY_RESULT_BACKEND = REDIS_URL
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:*@127.0.0.1:5432/mtproxyshop'


def get_config(name):
    conf_settings = os.environ.get('SETTINGS')
    _module, _class = conf_settings.split('.')
    the_module = import_module(_module)
    the_class = getattr(the_module, _class)
    return getattr(the_class, name)
