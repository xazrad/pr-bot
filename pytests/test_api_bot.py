import pytest

class TestPublicAPI:
    url = '/v1/order/'

    def test_start_0(self, client):
        # resp = client.post(self.url, data={'email': 'xazrad@gmail.com', 'lang': 'ru'})
        # assert resp.status_code == 200
        #
        # resp = client.post(self.url, data={'email': 'xazrad@gmail.com', 'lang': 'en'})
        # assert resp.status_code == 200

        resp = client.post(self.url, data={'email': 'xazrad@gmail.com', 'lang': 'ir'})
        assert resp.status_code == 200

        resp = client.post(self.url, data={'email': 'xazrad@gmail.com', 'lang': 'ru-RU'})
        assert resp.status_code == 200


class TestCommand:
    url = '/v1/botgram/'

    @pytest.mark.parametrize("file_name, status", [
        ('start.json', 200),
        ('start__create.json', 200),
        ('start__server_list.json', 200),
        ('start__language.json', 200),
        ('start__help.json', 200),
        
        ('language.json', 200),
        ('language__language.json', 200),
        
        ('help__back.json', 200),
        
        ('server__create.json', 200),
        
        ('server_view.json', 200),
        ('server_view__dns_add.json', 200),
        ('server_view__change_ip.json', 200),
        ('server_view__back.json', 200),
        ('server_view__confirm.json', 200),
        ('server_view__check_rkn.json', 200),
        ('change_ip__confirm.json', 200),
    ])
    def test_start(self, client, read_file, file_name, status):
        data = read_file(file_name)
        resp = client.post(self.url, data=data)
        assert resp.status_code == status
