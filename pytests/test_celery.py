from celery import chain

from celery_app import tasks


class TestCreateServerAndServices:
    def test_0(self, celery_app):
        tasks_list = list()
        tasks_list.append(tasks.create_droplet.s())  # создать сервер
        tasks_list.append(tasks.waiting_power_on_droplet.s(chat_id=423352934, port=443))
        tasks_list.append(tasks.install_service.s(test_period=20))
        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert result


class TestChangeTag:

    def test_0(self, celery_app):
        tasks_list = list()
        tasks_list.append(tasks.change_tag.s(2, '9e33acbf53239bffdd68a7215e8cd86b'))
        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert result


class TestOther:
    def test_0(self, celery_app):
        tasks_list = list()
        tasks_list.append(tasks.notify_user_error.s(3))
        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert result

    def test_1(self, celery_app):
        tasks_list = list()
        tasks_list.append(tasks.notify_user_email.s({'ip': '127.0.0.1', 'id': 1}, 'xazrad@gmail.com', 'server_installed', 'ru'))
        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert result


class TestCoingateVerify:
    def test_0(self, celery_app):
        form = {'receive_amount': ['0.000517'], 'receive_currency': ['BTC'], 'created_at': ['2018-07-23T08:33:53+00:00'],
                'order_id': ['MCw0MjMzNTI5MzQsODA='], 'pay_amount': ['0.000522'], 'pay_currency': ['BTC'],
                'token': ['Yzszb9HZaNbUXFvy61ByvZvj7teWYg'], 'id': ['102680'], 'status': ['paid'],
                'price_currency': ['USD'], 'price_amount': ['3.99']}
        tasks_list = list()
        tasks_list.append(tasks.verify_coingate_payment.s(form))
        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert result


class TestPaypalVerify:
    def test_0(self, celery_app):
        form = {'r': 44}

        tasks_list = list()
        tasks_list.append(tasks.verify_paypal_payment.s(form, sandbox=True))
        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert result


class TestLocalNotify:

    def test_0(self, celery_app):
        tasks_list = list()
        # tasks_list.append(tasks.notify_user.si({'id': 2}, chat_id=423352934, action='server_installed', locale='ru'))
        # tasks_list.append(tasks.notify_user.si({'id': 2}, chat_id=423352934, action='server_installed', locale='en'))
        # tasks_list.append(tasks.notify_user.si({'ip': 2, 'port': 2}, chat_id=423352934, action='tag_established', locale='ru'))
        # tasks_list.append(tasks.notify_user.si({'ip': 2, 'port': 2}, chat_id=423352934, action='tag_established', locale='en'))
        # tasks_list.append(tasks.notify_user.si({'ip': 2, 'port': 2}, chat_id=423352934, action='tag_removed', locale='ru'))
        # tasks_list.append(tasks.notify_user.si({'ip': 2, 'port': 2}, chat_id=423352934, action='trial_expired'))
        tasks_list.append(tasks.notify_user.si({'ip': 2, 'port': 2}, chat_id=423352934, action='warning_trial_expired'))
        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert True


class TestPeriodicTask:

    def test_check_trial(self, celery_app):
        tasks_list = list()
        tasks_list.append(tasks.check_trial.si(136,))

        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert True

    def test_removing_unused_servers(self, celery_app):
        tasks_list = list()
        tasks_list.append(tasks.removing_unused_servers.si())

        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert True

    def test_stop_unpaid_services(self, celery_app):
        tasks_list = list()
        tasks_list.append(tasks.stop_unpaid_services.si())

        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert True


def test_alert_black_friday(celery_app):
    tasks_list = list()
    tasks_list.append(tasks.alert_black_friday.s())

    result = chain(*tasks_list).apply_async().get(timeout=160)
    assert True


def test_check_blocked_servers(celery_app):
    # tasks.check_blocked_servers.apply_async().get(timeout=160)
    tasks.check_blocked_servers.apply_async()
    assert True


def test_delete_droplet():
    from vultr import Vultr
    VULTRAPI = Vultr('TOYJM4MMKSIEXP234IJ2BEBEZSOWF2OTQIHA')
    r = VULTRAPI.server.destroy(subid=22887941)
    assert True


def test_check_server_agent_callback(celery_app):
    tasks.check_server_agent_callback.apply_async(args=({'server_id': 90, 'is_blocked': True, 'ip': '8.8.8.8'},))
    assert True
