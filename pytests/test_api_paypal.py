from celery import chain

from celery_app import tasks


class TestCommand:
    url = '/v1/botgram/'

    def test_yandex_vire(self, client, add_command_reply):
        resp = client.post(self.url, data=add_command_reply)
        assert resp.status_code == 200

    # def test_0_post(self, client, add_command):
    #     resp = client.post(self.url, data=add_command)
    #     assert resp.status_code == 200

    # def test_1_post(self, client, add_command_reply):
    #     resp = client.post(self.url, data=add_command_reply)
    #     assert resp.status_code == 200

    def test_0(self, celery_app):
        form = {'receiver_id': ['PEBFBZUQB4DLN'], 'item_name': ['Rent MTProxy Service'], 'notify_version': ['3.9'], 'payment_date': ['12:09:34 Jul 03, 2018 PDT'], 'mc_fee': ['19.71'], 'transaction_subject': [''], 'quantity': ['1'], 'payment_gross': [''], 'test_ipn': ['1'], 'txn_id': ['3P53758958609640B'], 'payment_status': ['Completed'], 'last_name': ['buyer'], 'mc_currency': ['RUB'], 'receiver_email': ['xazrad-facilitator@yandex.ru'], 'item_number': [''], 'business': ['xazrad-facilitator@yandex.ru'], 'mc_gross': ['249.00'], 'protection_eligibility': ['Eligible'], 'verify_sign': ['ADkIKjbL1YNLtEWtWpHC8-dQVqx9A26AHcCv9.LcukwN0nuBCCyDiIAT'], 'custom': ['MCw0MjMzNTI5MzQsNjA='], 'ipn_track_id': ['d91e3dba9fcaa'], 'payer_id': ['3FTN49D7MVBUU'], 'payer_status': ['verified'], 'payment_fee': [''], 'payer_email': ['xazrad-buyer@yandex.ru'], 'charset': ['KOI8_R'], 'txn_type': ['web_accept'], 'residence_country': ['RU'], 'first_name': ['test'], 'payment_type': ['instant']}
        tasks_list = list()
        tasks_list.append(tasks.verify_paypal_payment.s(form, sandbox=True))
        result = chain(*tasks_list).apply_async().get(timeout=160)
        assert result
