from itsdangerous import TimestampSigner


class TestSign:

    def test_get(self, client):
        SECRET_KEY = '04e7ca68-b976-4a60-a840-a98dcd77ebe2'
        signer = TimestampSigner(SECRET_KEY)
        s = signer.sign('423352934').decode()
        url = '/ref/sign/{}/'.format(s)
        resp = client.get(url)
        assert resp.status_code == 302
