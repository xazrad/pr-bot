import os

import pytest

from app.factory import create_app
from celery_app.celery import app as app_celery

dir = os.path.dirname(os.path.abspath(__file__))

PATH = os.path.join(dir, 'data')


@pytest.fixture(scope='session')
def app():
    the_app = create_app()
    the_app.config['TESTING'] = True
    the_app.config['DEBUG'] = True
    return the_app


# Celery fixtures
@pytest.fixture(scope='module')
def celery_app():
    app_celery.conf.update(CELERY_ALWAYS_EAGER=True)
    # app_celery.conf.update(CELERY_EAGER_PROPAGATES_EXCEPTIONS=True)
    return app_celery


@pytest.fixture
def read_file():
    def _handler(file_name):
        the_path = os.path.join(PATH, file_name)
        with open(the_path, 'r') as f:
            return f.read()
    return _handler
