import os

import raven
from celery import Celery
from raven.contrib.celery import register_signal, register_logger_signal
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from app.factory import create_app
from config import get_config


flask_app = create_app(register_blueprints=False)
flask_app.app_context().push()


class AppCelery(Celery):

    def on_configure(self):
        client = raven.Client(
            'https://fbc7b738b6fc47eeaaecaf0eaea2e4eb:14a303f91b8c47f1a39d35721549a4f8@sentry.io/1227282')
        if not flask_app.config['DEBUG']:
            # register a custom filter to filter out duplicate logs
            register_logger_signal(client)
            #   hook into the Celery error handler
            register_signal(client)


def make_celery(app):
    celery_ = AppCelery('celery_app')
    celery_.config_from_object(os.environ.get('SETTINGS'))

    TaskBase = celery_.Task
    celery_.app = app

    class ContextTask(TaskBase):
        abstract = True
        _session = None

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

        def after_return(self, status, retval, task_id, args, kwargs, einfo):
            if self._session is not None:
                self._session.remove()

        @property
        def session(self):
            if self._session is None:
                engine = create_engine(get_config('SQLALCHEMY_DATABASE_URI'), convert_unicode=True)
                self._session = scoped_session(sessionmaker(engine))
            return self._session

    celery_.Task = ContextTask
    return celery_


app = make_celery(flask_app)

if __name__ == '__main__':
    with flask_app.app_context():
        app.start()
