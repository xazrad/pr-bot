import base64
import os
import random
import socket
from datetime import datetime, timedelta, time
from urllib import parse

import CloudFlare
import requests
import telepot
from emoji import emojize
from celery import chain, group
from  CloudFlare.exceptions import CloudFlareAPIError as Route53Error
from hetznercloud import HetznerCloudClientConfiguration, HetznerCloudClient
from ssh2.session import Session
from ssh2.exceptions import ChannelError, SocketRecvError
from vultr import Vultr
from sqlalchemy import func, and_
from sqlalchemy.orm import joinedload
from werkzeug.datastructures import ImmutableMultiDict

from celery.utils.log import get_task_logger
from flask_babel import gettext as _
from flask_babel import force_locale
from flask import current_app

from app.extensions import redis_store
from app.api.utils import generate_hex_string, send_email, full_url_payment
from celery_app.celery import app
from config import get_config
from .exceptions import ActionDoesNotReady, DockerNotReady
from .utils import generate_name


logger = get_task_logger(__name__)

configuration = HetznerCloudClientConfiguration().with_api_key(get_config('HETZNER_SECRET')).with_api_version(1)

CLIENT = HetznerCloudClient(configuration)

VULTRAPI = vultr = Vultr(get_config('VULT_ACCESS_TOKEN'))

REGIONS_VULTR = {
    7: 'AMS',
    8: 'GB',
    24: 'FR',
    9: 'DE'
}

ROUTE53 = CloudFlare.CloudFlare(email=get_config('CLOUDFLARE_EMAIL'), token=get_config('CLOUDFLARE_TOKEN'))


@app.task(bind=True)
def debug_task(self):
    logger.info('Request: {0!r}'.format(self.request))


@app.task(bind=True)
def debug_periodic_task(self):
    with open('/tmp/debug_periodic_task', 'w') as f:
        f.write('{0}: Request: {1!r}'.format(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'), self.request))


@app.task(bind=True)
def verify_coingate_payment(self, form):
    '''
    {'receive_amount': ['0.000517'],
    'receive_currency': ['BTC'],
    'created_at': ['2018-07-23T08:33:53+00:00'],
    'order_id': ['MCw0MjMzNTI5MzQsODA='],
    'pay_amount': ['0.000522'],
    'pay_currency': ['BTC'],
    'token': ['Yzszb9HZaNbUXFvy61ByvZvj7teWYg'],
    'id': ['102680'],
    'status': ['paid'],
    'price_currency': ['USD'],
    'price_amount': ['3.99']}
    :param self:
    :return:
    '''
    form = ImmutableMultiDict(form)
    status = form['status']  # confirming' or 'paid'

    from app.models import CoingateTxn, Service
    if status == 'paid':
        q = self.session.query(CoingateTxn).filter_by(id=form['id']).first()
        if q is not None:
            q.status = 'paid'
            self.session.commit()
            return

    bot_version, bot_chat_id, bot_service_id, = base64.urlsafe_b64decode(form['order_id']).decode().split(',')
    if bot_version == '0':
        bot_email = None
    else:
        bot_email = bot_chat_id
        bot_chat_id = None

    # проверим что сервис неудален
    service_obj = self.session.query(Service).get(bot_service_id)
    comment = None
    if service_obj.date_out:
        # оплачен удаленный сервис
        comment = 'Оплачен удаленный сервис!!!!'

        # создадим запись транзакции
    txn_obj = CoingateTxn(
        id=form['id'],
        bot_version=bot_version,
        bot_chat_id=bot_chat_id,
        bot_email=bot_email,
        bot_service_id=bot_service_id,
        comment=comment,
        order_id=form['order_id'],
        status=form['status'],
        price_amount=form['price_amount'],
        price_currency=form['price_currency'],
        receive_currency=form['receive_currency'],
        receive_amount=form['receive_amount'],
        pay_amount=form['pay_amount'],
        pay_currency=form['pay_currency'],
        created_at=form['created_at'],
    )

    # продлим payeied и если триал уберем его
    prepaid = service_obj.prepaid + timedelta(days=30)
    prepaid = datetime.combine(prepaid, time.max)
    service_obj.trial = False
    service_obj.prepaid = prepaid

    self.session.add(txn_obj)
    self.session.commit()

    tasks_list = list()
    if service_obj.chat_id:
        tasks_list.append(notify_user.si({'ip': service_obj.ip,
                                          'port': service_obj.port,
                                          'error': service_obj.date_out},
                                         service_obj.chat_id,
                                         action='payment_received'))

    if service_obj.email_order:
        tasks_list.append(notify_user_email.si({'ip': service_obj.ip,
                                                'port': service_obj.port,
                                                'error': service_obj.date_out},
                                               service_obj.email_order,
                                               action='payment_received'))
    group(*tasks_list).apply_async()


@app.task(bind=True)
def verify_paypal_payment(self, form, sandbox=False):
    '''
      example notify
      {'item_name': ['Rent MTProxy Service'],
      'quantity': ['1'],
      'mc_gross': ['249.00'],
      'ipn_track_id': ['e459ddaee8677'],
      'receiver_id': ['PEBFBZUQB4DLN'],
      'payer_status': ['verified'],
      'txn_type': ['web_accept'],
      'transaction_subject': [''],
      'test_ipn': ['1'],
      'receiver_email': ['xazrad-facilitator@yandex.ru'],
      'protection_eligibility': ['Eligible'],
      'notify_version': ['3.9'],
      'payment_status': ['Completed'],
      'payment_type': ['instant'],
      'mc_fee': ['19.71'],
      'item_number': [''],
      'payment_gross': [''],
      'verify_sign': ['ALFvbt9vjkC9JR7vWT.n8KTzx2nEAKj3fxlWtdWap7DUuxMeS4DVG8PV'],
      'residence_country': ['RU'],
      'payment_date': ['02:20:03 Jun 27, 2018 PDT'],
      'custom': ['MCw0MjMzNTI5MzQsMg=='],
      'first_name': ['test'],
      'mc_currency': ['RUB'],
      'payment_fee': [''],
      'last_name': ['buyer'],
      'charset': ['KOI8_R'],
      'payer_email': ['xazrad-buyer@yandex.ru'],
      'payer_id': ['3FTN49D7MVBUU'],
      'business': ['xazrad-facilitator@yandex.ru'],
      'txn_id': ['8A8179658N6623532']}

    :param form:
    :param sandbox:
    :return:
    '''
    config_name = 'PAYPAL_VERIFY_URL_TEST' if sandbox else 'PAYPAL_VERIFY_URL_PROD'
    url = current_app.config[config_name]

    form = ImmutableMultiDict(form)
    headers = {'content-type': 'application/x-www-form-urlencoded',
               'user-agent': 'Python-IPN-Verification-Script'}

    query = 'cmd=_notify-validate'
    for key, value in form.items():
        query += '&%s=%s' % (key, parse.quote(value))

    r = requests.post(url, data=query.encode(form['charset']), headers=headers, verify=True)
    r.raise_for_status()

    logger.info(r.text)
    if r.text == 'INVALID':
        logger.error('INVALID VERIFY: %s', query)
        return

    if r.text != 'VERIFIED':
        raise Exception(r.text)

    # проверим что такого платежа нет
    from app.models import PaypalTxn, Service

    q = self.session.query(PaypalTxn).filter_by(txn_id=form['txn_id'])
    if self.session.query(q.exists()).scalar():
        return

    bot_version, bot_chat_id, bot_service_id, = base64.urlsafe_b64decode(form['custom']).decode().split(',')
    if bot_version == '0':
        bot_email = None
    else:
        bot_email = bot_chat_id
        bot_chat_id = None

    # проверим что сервис неудален
    service_obj = self.session.query(Service).get(bot_service_id)
    comment = None
    if service_obj.date_out:
        # оплачен удаленный сервис
        comment = 'Оплачен удаленный сервис!!!!'

    # создадим запись транзакции
    txn_obj = PaypalTxn(
        txn_id=form['txn_id'],
        bot_version=bot_version,
        bot_chat_id=bot_chat_id,
        bot_email=bot_email,
        bot_service_id=bot_service_id,
        item_name=form.get('item_name', ''),
        quantity=form.get('quantity', ''),
        mc_gross=form['mc_gross'],
        test_ipn='test_ipn' in form,
        residence_country=form.get('residence_country', ''),
        mc_currency=form['mc_currency'],
        payment_date=form['payment_date'],
        payer_name='{} {}'.format(form.get('first_name', ''), form.get('last_name', '')),
        payer_email=form.get('payer_email', ''),
        business=form.get('business', ''),
        comment=comment
    )

    # продлим payeied и если триал уберем его
    prepaid = service_obj.prepaid + timedelta(days=30)
    prepaid = datetime.combine(prepaid, time.max)
    service_obj.trial = False
    service_obj.prepaid = prepaid

    self.session.add(txn_obj)
    self.session.commit()

    tasks_list = list()
    if service_obj.chat_id:
        tasks_list.append(notify_user.si({'ip': service_obj.ip,
                                          'port': service_obj.port,
                                          'error': service_obj.date_out},
                                         service_obj.chat_id,
                                         action='payment_received'))

    if service_obj.email_order:
        tasks_list.append(notify_user_email.si({'ip': service_obj.ip,
                                                'port': service_obj.port,
                                                'error': service_obj.date_out},
                                               service_obj.email_order,
                                               action='payment_received'))
    group(*tasks_list).apply_async()


@app.task(bind=True)
def vefify_yandex_payment(self, form):
    '''
         {'currency': ['643'],
         'label': ['MCw0MjMzNTI5MzQsMTg0'],
         'firstname': [''],
         'withdraw_amount': ['10.00'],
         'suite': [''],
         'email': [''],
         'sha1_hash': ['9c5862027f64e1524a6f17ac7e4eefe3764531b8'],
         'notification_type': ['card-incoming'],
         'unaccepted': ['false'],
         'datetime': ['2019-03-05T20:39:43Z'],
         'operation_id': ['605133583478041012'],
         'zip': [''], 'building': [''], 'phone': [''], 'flat': [''],
         'amount': ['9.80'],
         'lastname': [''],
         'codepro': ['false'], 'city': [''], 'sender': [''], 'fathersname': [''],
         'operation_label': ['2410f235-0011-5000-9000-1686f41d84ba'], 'street': ['']}

    :param self:
    :param form:
    :return:
    '''
    form = ImmutableMultiDict(form)

    from app.models import YandexMoneyTxn, Service

    try:
        bot_version, bot_chat_id, bot_service_id, = base64.urlsafe_b64decode(form['label']).decode().split(',')
    except:
        return
    if bot_version == '0':
        bot_email = None
    else:
        bot_email = bot_chat_id
        bot_chat_id = None
    # проверим что сервис неудален
    service_obj = self.session.query(Service).get(bot_service_id)
    comment = None
    if service_obj.date_out:
        # оплачен удаленный сервис
        comment = 'Оплачен удаленный сервис!!!!'

    # создадим запись транзакции
    txn_obj = YandexMoneyTxn(
        bot_version=bot_version,
        bot_chat_id=bot_chat_id,
        bot_email=bot_email,
        bot_service_id=bot_service_id,
        comment=comment,

        withdraw_amount=form['withdraw_amount'],
        amount=form['amount'],
        notification_type=form['notification_type'],
        unaccepted=form['unaccepted'],
        datetime=form['datetime'],
        operation_id=form['operation_id'],
        operation_label=form['operation_label'],
        codepro=form['codepro'],
    )

    # продлим payeied и если триал уберем его
    prepaid = service_obj.prepaid + timedelta(days=30)
    prepaid = datetime.combine(prepaid, time.max)
    service_obj.trial = False
    service_obj.prepaid = prepaid

    self.session.add(txn_obj)
    self.session.commit()
    tasks_list = list()
    if service_obj.chat_id:
        tasks_list.append(notify_user.si({'ip': service_obj.ip,
                                          'port': service_obj.port,
                                          'error': service_obj.date_out},
                                         service_obj.chat_id,
                                         action='payment_received'))

    if service_obj.email_order:
        tasks_list.append(notify_user_email.si({'ip': service_obj.ip,
                                                'port': service_obj.port,
                                                'error': service_obj.date_out},
                                               service_obj.email_order,
                                               action='payment_received'))
    group(*tasks_list).apply_async()


@app.task(bind=True, autoretry_for=(ActionDoesNotReady, socket.timeout),
          retry_kwargs={'max_retries': 30, 'countdown': 15})
def waiting_power_on_droplet(self, server_data, port, chat_id=None, email_order=None, reinstall=None, auto_ip=False):
    '''

    :param self:
    :param server_data: {'SUBID': ''}
    :param port:
    :param chat_id:
    :param email_order:
    :return:
    '''
    from app.models import Server, Service

    droplet_id = server_data['SUBID']
    # отфильтруем из списка
    api_data = [_[1] for _ in VULTRAPI.server.list().items() if _[0] == droplet_id][0]
    status = api_data['status']
    if status == 'pending':
        raise ActionDoesNotReady()
    if status != 'active':
        raise Exception('Invalid status %s' % status)

    server_obj = Server(
        droplet_id=droplet_id,
        ip=api_data['main_ip'],
        name=generate_name(),
        data_center=api_data['location'],
        status='done',
    )

    service_obj = Service(
        server=server_obj,
        chat_id=chat_id,
        email_order=email_order,
        ip=server_obj.ip,
        port=port,
        auto_ip=auto_ip,
        trial=False if reinstall else True,
    )
    if reinstall:
        service_obj_old = self.session.query(Service).get(reinstall)
        service_obj.secret_key = service_obj_old.secret_key
        service_obj.name = service_obj_old.name
        service_obj.prepaid = service_obj_old.prepaid

    self.session.add(service_obj)
    self.session.add(server_obj)
    self.session.commit()
    return service_obj.id


@app.task(bind=True)
def create_droplet(self):
    """
    :param self:
    :return: {'SUBID': '....int...'}
    """
    # https://api.vultr.com/v1/regions/list
    # 201 - тариф план
    VPSPLANID = 201
    available_locations = VULTRAPI.plans.list()['201']['available_locations']
    DCID = random.choice(list(filter(lambda x: x in REGIONS_VULTR.keys(), available_locations)))
    r = VULTRAPI.server.create(dcid=DCID, vpsplanid=VPSPLANID, osid='186', params={
        'APPID': 37,
        'SSHKEYID': '5c794f9f09e7f,5c794fd289de3,5c794feb7275b,5c7968161534a'
    })
    return r


@app.task
def destroy_droplet(droplet_id, provider):
    if provider == 'Hetzner':
        server = CLIENT.servers().get(int(droplet_id))
        server.delete()
    else:
        VULTRAPI.server.destroy(subid=droplet_id)


@app.task(bind=True, autoretry_for=(ActionDoesNotReady,
                                    DockerNotReady,
                                    ChannelError,
                                    SocketRecvError,
                                    TimeoutError,
                                    OSError,
                                    socket.timeout), retry_kwargs={'max_retries': 30, 'countdown': 10})
def install_docker(self, server_id, port, email_order=None, chat_id=None):
    """
    Задача в настоящее время не используется
    """
    from app.models import Server, Service

    server_obj = self.session.query(Server).get(server_id)
    server = CLIENT.servers().get(server_obj.droplet_id)
    if server.status != 'running':
        raise ActionDoesNotReady()

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((server.public_net_ipv4, 22))
    session = Session()
    session.handshake(sock)
    session.userauth_publickey_fromfile('root', str(current_app.config['KEYS_DIR'] / 'spp_key'))

    channel = session.open_session()
    channel.execute('apt-get -y update && apt-get -y upgrade')
    _, _ = channel.read()
    channel.close()

    server_obj.status = 'done'

    service_obj = Service(
        server_id=server_id,
        chat_id=chat_id,
        email_order=email_order,
        ip=server_obj.ip,
        port=port,
        trial=True,
    )
    self.session.add(service_obj)
    self.session.commit()
    return service_obj.id



@app.task(bind=True, autoretry_for=(socket.timeout,
                                    TimeoutError,
                                    OSError,
                                    SocketRecvError,
                                    ChannelError), retry_kwargs={'max_retries': 30, 'countdown': 10})
def install_service(self, service_id, test_period, is_new=True, tag=''):
    from app.models import Service

    service_obj = self.session.query(Service).get(service_id)
    secret_key = service_obj.secret_key or generate_hex_string()
    name = service_obj.name or generate_name()
    port = service_obj.port

    if tag:
        cmd_install = 'docker run -p{0}:443 -v proxy-config-{1}:/data ' \
                      '-e SECRET={2} -e TAG={3} --restart=unless-stopped ' \
                      '--log-opt max-size=5m --log-opt max-file=3 --name {1} ' \
                      '-d sproxy/mtp:latest'.format(port, name, secret_key, tag or '')
    else:
        cmd_install = 'docker run -p{0}:443 -v proxy-config-{1}:/data ' \
                      '-e SECRET={2} --restart=unless-stopped ' \
                      '--log-opt max-size=5m --log-opt max-file=3 --name {1} ' \
                      '-d sproxy/mtp:latest'.format(port, name, secret_key)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((service_obj.ip, 22))
    session = Session()
    session.handshake(sock)
    session.userauth_publickey_fromfile('root', str(current_app.config['KEYS_DIR'] / 'spp_key'))

    channel = session.open_session()
    channel.execute(cmd_install)
    _, _ = channel.read()
    channel.close()

    prepaid = service_obj.prepaid or datetime.utcnow() + timedelta(minutes=test_period)

    service_obj.name = name
    service_obj.secret_key = secret_key
    service_obj.status = 'done'
    service_obj.prepaid = prepaid
    service_obj.tag = tag or None
    self.session.commit()
    # resp = {_:service_obj.__dict__[_] for _ in service_obj.__dict__ if not _.startswith('_')}
    # поставим задачу на выполнение через
    if is_new:
        warning_check_trial.apply_async((service_id,), eta=prepaid - timedelta(minutes=20))
        check_trial.apply_async((service_id,), eta=prepaid)

    return {
        'id': service_obj.id,
        'name': service_obj.name,
        'ip': service_obj.ip,
        'port': service_obj.port,
        'secret_key': service_obj.secret_key,
        'commit': service_obj.commit
    }


@app.task(autoretry_for=(DockerNotReady,
                         ChannelError,
                         SocketRecvError,
                         TimeoutError,
                         OSError,
                         socket.timeout), retry_kwargs={'max_retries': 30, 'countdown': 10})
def destroy_service(ip, docker_name):
    cmd = 'docker stop {0} && docker rm {0}'.format(docker_name)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, 22))
    session = Session()
    session.handshake(sock)
    session.userauth_publickey_fromfile('root', str(current_app.config['KEYS_DIR'] / 'spp_key'))

    channel = session.open_session()
    channel.execute(cmd)
    _, _ = channel.read()
    channel.close()


@app.task(bind=True, autoretry_for=(DockerNotReady,
                                    ChannelError,
                                    SocketRecvError,
                                    TimeoutError,
                                    OSError,
                                    socket.timeout), retry_kwargs={'max_retries': 50, 'countdown': 10})
def change_tag(self, service_id, tag=None):
    from app.models import Service

    service_obj = self.session.query(Service).options(
        joinedload(Service.server, innerjoin=True)
    ).get(service_id)

    if tag:
        cmd = 'docker rm -f {1} && ' \
              'docker run -p{0}:443 -v proxy-config-{1}:/data ' \
              '-e SECRET={2} -e TAG={3} --restart=unless-stopped ' \
              '--log-opt max-size=5m --log-opt max-file=3 --name {1} ' \
              '-d sproxy/mtp:latest'.format(service_obj.port,
                                            service_obj.name,
                                            service_obj.secret_key,
                                            tag or '')
    else:
        cmd = 'docker rm -f {1} && ' \
              'docker run -p{0}:443 -v proxy-config-{1}:/data ' \
              '-e SECRET={2} --restart=unless-stopped ' \
              '--log-opt max-size=5m --log-opt max-file=3 --name {1} ' \
              '-d sproxy/mtp:latest'.format(service_obj.port,
                                            service_obj.name,
                                            service_obj.secret_key)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((service_obj.server.ip, 22))
    session = Session()
    session.handshake(sock)
    session.userauth_publickey_fromfile('root', str(current_app.config['KEYS_DIR'] / 'spp_key'))

    channel = session.open_session()
    channel.execute(cmd)
    _, _ = channel.read()
    channel.close()

    service_obj.tag = tag
    self.session.commit()


@app.task(bind=True)
def notify_user(self, data, chat_id, action=None, locale=None, redis_key=None):
    from app.models import Chat

    if redis_key:
        redis_store.delete(redis_key)

    if locale is None:
        # берем из настроек чата
        locale = self.session.query(Chat).get(chat_id).locale

    bot = telepot.Bot(current_app.config['BOT_TOKEN'])
    buttons = []
    text = 'Message'
    if action == 'server_installed':
        with force_locale(locale):
            text = _('A new server is installed!')
            buttons = [
                [
                    {'text': _('View'),
                     'callback_data': "/list,server,{}".format(data['id'])
                     }
                ],
            ]

    if action == 'tag_established':
        with force_locale(locale):
            text = [
                _('Server %s:%s') % (data['ip'], data['port']),
                _('Tag is established.')
            ]

    if action == 'tag_removed':
        with force_locale(locale):
            text = [
                _('Server %s:%s') % (data['ip'], data['port']),
                _('Tag is removed.')
            ]

    if action == 'trial_expired':
        with force_locale(locale):
            text = [
                _('Server %s:%s') % (data['ip'], data['port']),
                _('A free test period is expired.'),
                _('The server will be deleted.')
            ]

    if action == 'warning_trial_expired':
        with force_locale(locale):
            text = [
                _('Server %s:%s') % (data['ip'], data['port']),
                _('Free trial period ends in an 20 minutes.'),
                _('Please make payment.')
            ]
            buttons = [
                [{'text': _('Renew rent for 30 days'),
                 'callback_data': "/server,payment,{}".format(data['id'])}
                 ]
            ]

    if action == 'payment_received':
        with force_locale(locale):
            if data['error']:
                text = [
                    _('Server %s:%s') % (data['ip'], data['port']),
                    _('Paid invalid account.'),
                    _('Contact Support.'),
                ]
            else:
                text = [
                    _('Server %s:%s') % (data['ip'], data['port']),
                    _('Rent extended for 30 days.'),
                ]

    if action == 'reminder_of_payment':
        with force_locale(locale):
            text = [
                _('Server %s:%s') % (data['ip'], data['port']),
                emojize(':face_screaming_in_fear: {}'.format(_('Risk of suspension of your server in a few days.'))),
                _('Pay rent to server.'),
            ]

    if action == 'check_rkn':
        with force_locale(locale):
            is_blocked = _('Yes! It appears that currently blocked in Russia.')
            is_not_blocked = _('No, Probably not blocked in Russia. Yet.')

            text = [
                'IP: %s' % data['ip'],
                is_blocked if data['is_blocked'] else is_not_blocked,
            ]
            buttons = [
                [
                    {'text': _('<< Back'),
                     'callback_data': "/list,server,{}".format(data['service_id'])
                     }
                ],
            ]

    if action == 'server_not_accessible':
        with force_locale(locale):
            text = [
                _('We detected your server is not accessible from Russia.'),
                'IP: %s' % data['ip'],
            ]
            buttons = [
                [
                    {'text': _('View'),
                     'callback_data': "/list,server,{}".format(data['service_id'])
                     }
                ]
            ]

            if not data['auto_ip']:
                buttons.extend([
                    [
                        {'text': _('Change IP'),
                        'callback_data': "/server,change_ip,{}".format(data['service_id'])
                        }
                    ],
                    [
                        {'text': _('Enable auto change IP'),
                        'callback_data': "/server,auto_change_ip,{}".format(data['service_id'])
                        }
                    ],
                ]
)
    if isinstance(text, list):
        text = '\n'.join(text)

    if buttons:
        try:
            bot.sendMessage(chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except (telepot.exception.BotWasBlockedError, telepot.exception.TelegramError) as e:
            pass
    else:
        try:
            bot.sendMessage(chat_id, text)
        except (telepot.exception.BotWasBlockedError, telepot.exception.TelegramError) as e:
            pass


@app.task
def notify_user_error(chat_id, locale='en', redis_key=None):
    bot = telepot.Bot(current_app.config['BOT_TOKEN'])

    if redis_key:
        redis_store.delete(redis_key)

    with force_locale(locale):
        text = emojize('{} :man_facepalming:'.format(_('Something wrong, please, try again later!')))

    try:
        bot.sendMessage(chat_id, text)
    except telepot.exception.BotWasBlockedError:
        pass


@app.task
def notify_user_email(data, email, action, locale='ru', redis_key=None, ):
    if redis_key:
        redis_store.delete(redis_key)

    data['server_name'] = current_app.config['SERVER_HOST']
    data['scheme'] = current_app.config['PREFERRED_URL_SCHEME']

    if action == 'server_installed':
        with force_locale(locale):
            subject = _('Server is installed')
        file_name = os.path.join(str(current_app.config['BASE_DIR']), 'app', 'api',
                                 'templates', 'email_server_installed.html')

        with open(file_name, 'r') as f:
            template_sting = f.read()
        data['payload'] = base64.urlsafe_b64encode('{},{}'.format(email,data['id']).encode()).rstrip(b'\n=').decode()
        data['bot_name'] = current_app.config['BOT_NAME']
        data['processing_url_paypal'] = full_url_payment(data['id'], payer={'type': 'email', 'value': email})
        data['processing_url_crypto'] = full_url_payment(data['id'], payer={'type': 'email', 'value': email}, method='crypto')
        send_email(email, subject, data, template_sting=template_sting, is_invoice=True)

    if action == 'trial_expired':
        with force_locale(locale):
            subject = _('A free test period is expired.')
        file_name = os.path.join(str(current_app.config['BASE_DIR']), 'app', 'api',
                                 'templates', 'email_trial_expired.html')

        with open(file_name, 'r') as f:
            template_sting = f.read()
        send_email(email, subject, data, template_sting=template_sting)

    if action == 'warning_trial_expired':
        with force_locale(locale):
            subject = _('Free trial period ends in an 20 minutes.')
        file_name = os.path.join(str(current_app.config['BASE_DIR']), 'app', 'api',
                                 'templates', 'email_warning_trial_expired.html')

        with open(file_name, 'r') as f:
            template_sting = f.read()
        data['processing_url_paypal'] = full_url_payment(data['id'], payer={'type': 'email', 'value': email})
        data['processing_url_crypto'] = full_url_payment(data['id'], payer={'type': 'email', 'value': email}, method='crypto')
        send_email(email, subject, data, template_sting=template_sting, is_invoice=True)

    if action == 'payment_received':
        with force_locale(locale):
            if data['error']:
                subject = _('Paid invalid account.')
            else:
                subject = _('Rent extended for 30 days.')
        file_name = os.path.join(str(current_app.config['BASE_DIR']), 'app', 'api',
                                 'templates', 'email_payment_received.html')

        with open(file_name, 'r') as f:
            template_sting = f.read()
        send_email(email, subject, data, template_sting=template_sting)

    if action == 'reminder_of_payment':
        with force_locale(locale):
            subject = _('Reminder about payment.')
        file_name = os.path.join(str(current_app.config['BASE_DIR']), 'app', 'api',
                                 'templates', 'email_reminder_of_payment.html')

        with open(file_name, 'r') as f:
            template_sting = f.read()
        data['processing_url_paypal'] = full_url_payment(data['id'], payer={'type': 'email', 'value': email})
        data['processing_url_crypto'] = full_url_payment(data['id'], payer={'type': 'email', 'value': email}, method='crypto')
        send_email(email, subject, data, template_sting=template_sting, is_invoice=True)


@app.task(bind=True)
def warning_check_trial(self, service_id):
    """
    Уведомление о скоро заверщении  триала
    :param self:
    :param service_id:
    :return:
    """
    from app.models import Service

    service_obj = self.session.query(Service).get(service_id)
    if not service_obj.trial:
        return
    tasks_list = []
    if service_obj.chat_id:
        tasks_list.append(notify_user.si({'ip': service_obj.ip,
                                         'port': service_obj.port,
                                          'id': service_obj.id},
                                        chat_id=service_obj.chat_id,
                                        action='warning_trial_expired'))
    if service_obj.email_order:
        tasks_list.append(notify_user_email.si({'ip': service_obj.ip,
                                                'port': service_obj.port,
                                                'id': service_obj.id},
                                               email=service_obj.email_order,
                                               action='warning_trial_expired'))

    group(*tasks_list).apply_async()


@app.task(bind=True)
def check_trial(self, service_id):
    from app.models import Service

    service_obj = self.session.query(Service).get(service_id)
    if not service_obj.trial:
        return
    service_obj.date_out = datetime.utcnow()

    tasks_list = []
    if service_obj.chat_id:
        tasks_list.append(notify_user.si({'ip': service_obj.ip,
                                         'port': service_obj.port},
                                        chat_id=service_obj.chat_id,
                                        action='trial_expired'))
    if service_obj.email_order:
        tasks_list.append(notify_user_email.si({'ip': service_obj.ip,
                                                'port': service_obj.port},
                                               email=service_obj.email_order,
                                               action='trial_expired'))

    tasks_list.append(destroy_service.si(service_obj.ip, service_obj.name))
    # проверим есть ли еще сервисы на сервере
    installed_services = self.session.query(Service).filter(
        Service.server_id == service_obj.server_id,
        Service.id != service_obj.id,
        Service.date_out == None
    ).count()
    if installed_services == 0:
        service_obj.server.date_out = datetime.utcnow()
        tasks_list.append(destroy_droplet.si(service_obj.server.droplet_id, service_obj.server.provider))
    # удалить записи dns
    tasks_list.append(remove_dns_name_amazon.si(service_obj.name))
    self.session.commit()
    chain(*tasks_list).apply_async()


# периодические задачм
@app.task(bind=True)
def reminder_of_payment(self):
    from app.models import Service
    # извещаем за 3 дня
    end_of_day = datetime.combine(datetime.utcnow().date(), time.max)
    before_day = end_of_day + timedelta(days=3)

    service_objs = self.session.query(Service).filter(
        Service.date_out == None,
        Service.trial == False,
        Service.prepaid <= before_day
    )
    tasks_list = list()

    for service_obj in service_objs:
        if service_obj.chat_id:
            tasks_list.append(
                notify_user.si({'ip': service_obj.ip,
                                'port': service_obj.port,
                                'id': service_obj.id,
                                'chat_id': service_obj.chat_id},
                               chat_id=service_obj.chat_id,
                               action='reminder_of_payment')
            )
        if service_obj.email_order:
            tasks_list.append(
                notify_user_email.si({'ip': service_obj.ip,
                                      'port': service_obj.port,
                                      'id': service_obj.id},
                                     email=service_obj.email_order,
                                     action='reminder_of_payment')
            )

    if tasks_list:
        group(*tasks_list).apply_async()


@app.task(bind=True)
def stop_unpaid_services(self):
    from app.models import Service

    # останавливаем из настройки OVERDUE_BEFORE_STOP_SERVICE_DAYS
    start_of_day = datetime.combine(datetime.utcnow().date(), time.min)
    deadline_day = start_of_day - timedelta(days=current_app.config['OVERDUE_BEFORE_STOP_SERVICE_DAYS'])

    service_objs = self.session.query(Service).filter(
        Service.date_out == None,
        Service.trial == False,
        Service.prepaid <= deadline_day
    )
    tasks_list = list()
    service_id_list = list()

    for service_obj in service_objs:
        tasks_list.append(
            destroy_service.si(service_obj.ip, service_obj.name)
        )
        # проверим есть ли еще сервисы на сервере
        installed_services = self.session.query(Service).filter(
            Service.server_id == service_obj.server_id,
            Service.id != service_obj.id,
            Service.date_out == None
        ).count()
        if installed_services == 0:
            service_obj.server.date_out = datetime.utcnow()
            tasks_list.append(destroy_droplet.si(service_obj.server.droplet_id, service_obj.server.provider))
        # удалить записи dns
        tasks_list.append(remove_dns_name_amazon.si(service_obj.name))
        service_id_list.append(service_obj.id)
    if tasks_list:
        group(*tasks_list).apply_async()
    date_out = datetime.utcnow()
    self.session.query(Service).filter(Service.id.in_(service_id_list)).update({'date_out': date_out},
                                                                               synchronize_session='fetch')
    self.session.commit()


@app.task(bind=True)
def removing_unused_servers(self):
    from app.models import Server, Service

    query = self.session.query(
        Server.id,
        Server.droplet_id,
        Server.provider,
        func.count(Service.id)
    )
    query = query.join(Service, and_(Service.server_id == Server.id, Service.date_out == None), isouter=True)
    query = query.filter(Server.status == 'done')
    query = query.filter(Server.date_out == None)
    query = query.group_by(Server.id)
    query = query.having(func.count(Service.id) == 0)
    query = query.all()

    tasks_list = list()

    date_out = datetime.utcnow()
    for row in query:
        server_id, droplet_id, provider, _count = row
        self.session.query(Server).filter_by(id=server_id).update({
            'date_out': date_out
        })
        tasks_list.append(destroy_droplet.si(droplet_id, provider))

    self.session.commit()
    if tasks_list:
        group(*tasks_list).apply_async()


@app.task(bind=True)
def alert_black_friday_send(self, chat_id):
    from app.models import ActionAlert

    bot = telepot.Bot(current_app.config['BOT_TOKEN'])
    text = (
        emojize(':Russia:'),
        '\n',
        'Новости обновления бота.\n',
        'https://t.me/iv?url=https%3A%2F%2Fmtproxy.shop%2Fru%2Frelease-060319.html&rhash=0dc636a1cbe364.\n',
    )
    text = ''.join(text)
    try:
        bot.sendMessage(chat_id, text)
    except (telepot.exception.BotWasBlockedError, telepot.exception.TelegramError) as e:
        status = 'ok'
    except Exception as e:
        status = 'error'
    else:
        status = 'ok'
    finally:
        alert_obj = ActionAlert(
            action='new_realise',
            status=status,
            chat_id=chat_id
        )
        self.session.add(alert_obj)
        self.session.commit()


@app.task(bind=True)
def alert_black_friday(self):
    from app.models import Chat, ActionAlert

    alerted = self.session.query(ActionAlert).filter_by(action='new_realise', status='ok').with_entities('chat_id')
    alerted = [_[0] for _ in alerted]
    query = self.session.query(Chat.id).filter(~Chat.id.in_(alerted)).limit(100)
    chat_id_list = [_[0] for _ in query]
    tasks_list = []
    for chat_id in chat_id_list:
        tasks_list.append(alert_black_friday_send.si(chat_id))
    group(*tasks_list).apply_async()


@app.task(bind=True)
def check_blocked_servers(self):
    from app.models import Server

    server_query = self.session.query(Server).filter_by(date_out=None,
                                                        status='done',
                                                        is_blocked=False).with_entities('id', 'ip')
    servers = [{'server_id': _.id, 'ip': _.ip}for _ in server_query]
    tasks_list = list()
    for server in servers:
        t = check_server_agent.si(**server)
        t.link(check_server_agent_callback.s())
        tasks_list.append(t)
    if tasks_list:
        group(*tasks_list).apply_async()


@app.task(bind=True)
def check_server_agent_callback(self, server_data):
    from app.models import Server, Service
    from app.api.utils import get_tasks_change_IP

    server_obj = self.session.query(Server).get(server_data['server_id'])

    server_obj.checked = datetime.utcnow()
    server_obj.is_blocked = server_data['is_blocked']
    self.session.commit()
    if not server_data['is_blocked']:
        return
    # уведомления
    service_list = self.session.query(Service).filter_by(
        date_out=None,
        server_id=server_data['server_id']).filter(Service.chat_id != None)
    tasks_list = list()
    for service in service_list:
        tasks_list.append(notify_user.si({'ip': server_data['ip'],
                                          'service_id': service.id,
                                          'auto_ip': service.auto_ip},
                                         service.chat_id,
                                         action='server_not_accessible'))
        # задачи на смену IP адреса
        redis_key = 'change_ip:chat:{}'.format(service.chat_id)
        if redis_store.get(redis_key):
            continue
        # только если включена опция auto_ip
        if not service.auto_ip:
            continue
        the_tasks = get_tasks_change_IP(self.session, service, service.chat, redis_key)
        chain(*the_tasks).apply_async(link_error=notify_user_error.si(chat_id=service.chat.id,
                                                                      locale=service.chat.locale,
                                                                      redis_key=redis_key))
    if tasks_list:
        group(*tasks_list).apply_async()


@app.task
def check_server_agent(server_id, ip):
    pass


@app.task
def check_service_agent(value, ip):
    pass


@app.task(autoretry_for=(Route53Error,), retry_kwargs={'max_retries': 20, 'countdown': 10})
def set_dns_name_amazon(data):
    dns_data = {
        'name': data['name'],
        'type': 'A',
        'content': data['ip'],
        'ttl': 120
    }
    ROUTE53.zones.dns_records.post(get_config('ROUTE53_ZONE_ID'), data=dns_data)

    return data


@app.task(autoretry_for=(Route53Error,), retry_kwargs={'max_retries': 20, 'countdown': 10})
def remove_dns_name_amazon(name):
    name = '{}.{}'.format(name, get_config('ROUTE53_DOMAIN'))
    zone_id = get_config('ROUTE53_ZONE_ID')
    record_sets = ROUTE53.zones.dns_records.get(zone_id, params={'name': name})
    for record in record_sets:
        ROUTE53.zones.dns_records.delete(zone_id, record['id'])


@app.task(autoretry_for=(Route53Error,), retry_kwargs={'max_retries': 20, 'countdown': 10})
def update_dns_name_amazon(data):
    name = '{}.{}'.format(data['name'], get_config('ROUTE53_DOMAIN'))
    zone_id = get_config('ROUTE53_ZONE_ID')
    record_sets = ROUTE53.zones.dns_records.get(zone_id, params={'name': name})
    for record in record_sets:
        ROUTE53.zones.dns_records.put(zone_id,
                                      record['id'],
                                      data={'name': name,
                                            'type': 'A',
                                            'content': data['ip'],
                                            'ttl': 120})
    return data
