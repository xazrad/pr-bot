��          �               �     �     �     �  	   �     	          5  (   K  (   t     �     �     �     �     �       0   &     W     d  )   x     �     �     �     �  6   �  1     �  P  
   (  >   3  -   r     �  :   �  -   �  &   &  e   M  Q   �  )     -   /  2   ]  )   �  .   �  6   �  �    	     �	  !   �	  Q   �	     *
     G
  %   \
     �
  ]   �
  C   �
   << Back A free test period is expired. A new server is installed! Change IP Contact Support. Domain name %s is attached Enable auto change IP Free trial period ends in an 20 minutes. No, Probably not blocked in Russia. Yet. Paid invalid account. Pay rent to server. Please make payment. Reminder about payment. Renew rent for 30 days Rent extended for 30 days. Risk of suspension of your server in a few days. Server %s:%s Server is installed Something wrong, please, try again later! Tag is established. Tag is removed. The server will be deleted. View We detected your server is not accessible from Russia. Yes! It appears that currently blocked in Russia. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-04-12 01:26+0300
PO-Revision-Date: 2018-06-26 21:38+0300
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: ru
Language-Team: ru <LL@li.org>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 Назад Истек бесплатный тестовый период. Установлен новый сервер! Сменить IP-адрес Свяжитесь со службой поддержки. Привязано доменное имя %s Включить автосмену IP Бесплатный пробный период заканчивается через 20 минут. Нет. Возможно не заблокирован в России. Пока. Оплачен неверный счет. Оплатите аренду сервера. Пожалуйста, внесите оплату. Напоминание об оплате. Продлить аренду на 30 дней Срок аренды продлен на 30 дней. Опасность приостановления работы вашего сервера через несколько дней. Сервер %s:%s Сервер установлен Что-то пошло не так. Повторите попытку позже! Тег установлен. Тег удален. Сервер будет удален. Открыть Мы определили что ваш сервер не доступен из России. Да! Похоже уже заблокирован в России. 