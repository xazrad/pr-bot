"""empty message

Revision ID: 4c7458a8fc96
Revises: ae04780aad87
Create Date: 2018-11-24 14:40:29.522243

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4c7458a8fc96'
down_revision = 'ae04780aad87'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('action_alert',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('action', sa.String(length=128), nullable=True),
    sa.Column('date_in', sa.DateTime(), nullable=True),
    sa.Column('status', sa.String(length=128), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('action_alert')
    # ### end Alembic commands ###
