"""empty message

Revision ID: ff2095900974
Revises: d6530c706109
Create Date: 2019-04-11 00:47:36.305374

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ff2095900974'
down_revision = 'd6530c706109'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('referral_account',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('balance', sa.INTEGER(), nullable=False),
    sa.Column('wallet', sa.String(length=14), nullable=True),
    sa.Column('utm', sa.String(length=24), nullable=True),
    sa.ForeignKeyConstraint(['id'], ['chat.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_referral_account_utm'), 'referral_account', ['utm'], unique=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_referral_account_utm'), table_name='referral_account')
    op.drop_table('referral_account')
    # ### end Alembic commands ###
