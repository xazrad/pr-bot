"""referral accounts

Revision ID: 5176fdaea541
Revises: ff2095900974
Create Date: 2019-04-11 00:55:23.205883

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5176fdaea541'
down_revision = 'ff2095900974'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("INSERT INTO referral_account (id, balance, wallet, utm) SELECT c.id, 0, NULL, c.id FROM chat c;")


def downgrade():
    pass
