"""alter refferal

Revision ID: 02933c3cfd6b
Revises: 30f55e5e7aa6
Create Date: 2019-04-12 20:12:19.906101

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '02933c3cfd6b'
down_revision = '30f55e5e7aa6'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute('alter table referral_account alter column wallet type varchar(18) using wallet::varchar(18);')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
