"""empty message

Revision ID: 5fd558c19cd5
Revises: 4c7458a8fc96
Create Date: 2019-02-21 00:34:25.011355

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5fd558c19cd5'
down_revision = '4c7458a8fc96'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute('alter table server alter column droplet_id type varchar(80) using droplet_id::varchar(80);')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
