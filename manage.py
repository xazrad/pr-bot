import os
import argparse

from flask_script import Manager
from flask_migrate import MigrateCommand

from app.factory import create_app

parser = argparse.ArgumentParser(description='Run project option')
parser.add_argument('action', choices=['run', 'db', 'locale'], help='action')
parser.add_argument('args', nargs=argparse.REMAINDER)


os.environ.setdefault('SETTINGS', 'config.Production')


if __name__ == '__main__':
    app = create_app()
    manager = Manager(app)
    manager.add_command('db', MigrateCommand)

    pars_args = parser.parse_args()
    if pars_args.action == 'run':
        app.run(host='127.0.0.1', port=8080)

    if pars_args.action == 'db':
        from app.models import *
        manager.run()
