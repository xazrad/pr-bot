import os

from app.factory import create_app


os.environ.setdefault('SETTINGS', 'config.Staging')

app = create_app()
