from itsdangerous import BadSignature, TimestampSigner

from flask import current_app, g, request


signer = TimestampSigner(current_app.config['SECRET_KEY'])


def get_user_cookie(f):
    def wrap(*args, **kwargs):
        user_id = request.cookies.get("user_id")
        if user_id:
            try:
                user_id = signer.unsign(user_id).decode()
            except BadSignature:
                user_id = None
        return f(*args, **kwargs, user_id=user_id)
    return wrap
