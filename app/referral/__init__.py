import json

from itsdangerous import BadSignature, TimestampSigner, SignatureExpired
from sqlalchemy.sql import func

from flask import Blueprint, jsonify, current_app, redirect, render_template, request, url_for

from app import BaseView
from app.extensions import db
from app.models import Chat, ReferralAccount, ReferralTransactions

from .decorators import get_user_cookie


referral_bp = Blueprint('referral', __name__,
                        template_folder='front/build',
                        static_url_path='/static',
                        static_folder='front/build/static')


signer = TimestampSigner(current_app.config['SECRET_KEY'])


class ReferralSignView(BaseView):
    methods = ['GET']

    def get(self, sign):
        resp = redirect(url_for('referral.ReferralView', _external=False))
        try:
            signer.unsign(sign, max_age=30)
        except (SignatureExpired, BadSignature):
            # удаляем куку
            resp.set_cookie('user_id', '', max_age=0)
        else:
            resp.set_cookie('user_id', sign, max_age=60 * 60 * 12)
        return resp


class ReferralView(BaseView):
    methods = ['GET', 'POST']

    @get_user_cookie
    def post(self, user_id, *args, **kwargs):
        """
        Метод для обновления Яндекс.Кошелька
        """
        r = dict(id=user_id)
        if user_id:
            r = request.get_json(force=True)
            r['id'] = user_id
            wallet = r['wallet']
            db.session.query(ReferralAccount).filter_by(id=user_id).update({'wallet': wallet})
            db.session.commit()
        return jsonify({'user': r})

    @get_user_cookie
    def get(self, user_id, *args, **kwargs):
        initial_state = {'user': {'id': None}}
        if user_id:
            ref_acount = db.session.query(ReferralAccount).get(user_id)
            initial_state['user'] = {
                'id': user_id,
                'utm': ref_acount.utm,
                'wallet': ref_acount.wallet,
                'cost': ref_acount.cost
            }

            query_progress = db.session.query(func.sum(ReferralTransactions.amount)).filter(ReferralTransactions.account_id == user_id)
            query_progress = query_progress.group_by(ReferralTransactions.account_id)
            accumulated = query_progress.filter(ReferralTransactions.type == 'credit').scalar() or 0  # всего начислено
            received = query_progress.filter(ReferralTransactions.type == 'debit').scalar() or 0  # получено
            receive = accumulated - received
            initial_state['progress'] = {
                'invited': db.session.query(Chat).filter_by(utm=ref_acount.utm).count(),  # приглашено пользователей
                'accumulated': accumulated,
                'received': received,
                'receive': receive
            }

            transactions = []
            query = db.session.query(ReferralTransactions).filter(ReferralTransactions.account_id == user_id).order_by(
                ReferralTransactions.date_in.desc())
            for row in query:
                transactions.append({
                    'type': row.type,
                    'amount': row.amount,
                    'date': row.date_in.isoformat()
                })
            initial_state['transactions'] = transactions
        initial_state = json.dumps(initial_state)
        # window.__INITIAL_STATE__= {{ initial_state |  safe}}
        return render_template('index.html', initial_state=initial_state)


@referral_bp.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = request.headers.get('ORIGIN', '')
    header['Access-Control-Allow-Credentials'] = 'true'
    header['Access-Control-Allow-Headers'] = 'origin, content-type, accept, x-requested-with'
    return response


referral_bp.add_url_rule('/sign/<string:sign>/', view_func=ReferralSignView.as_view('ReferralSignView'))
referral_bp.add_url_rule('/<path:path>', view_func=ReferralView.as_view('ReferralView2'))
referral_bp.add_url_rule('/', view_func=ReferralView.as_view('ReferralView'))
