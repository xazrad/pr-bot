import re
from datetime import timedelta

import requests
import telepot
from celery import chain
from flask_babel import gettext as _
from flask_babel import get_locale
from flask import Blueprint, current_app, jsonify, render_template, request, url_for

from app import BaseView
from app.extensions import db, redis_store
from app.models import Service
from celery_app import tasks

from .commands import commands_avalible
from .decorators import write_user_info
from .utils import find_server_for_install, generate_hex_string, send_email


api_v1 = Blueprint('v1', __name__, template_folder='templates')


class BotView(BaseView):
    methods = ['POST', 'GET']

    def get(self):
        return '', 200

    @write_user_info
    def post(self, message, callback_query):
        if callback_query:
            command_name, callback, value = callback_query.split(',')
            bot = telepot.Bot(current_app.config['BOT_TOKEN'])
            try:
                bot.deleteMessage((message['chat']['id'], message['message_id']))
            except:
                pass
            try:
                bot.sendChatAction(message['chat']['id'], 'typing')
            except:
                pass
            command_handler_cls = commands_avalible.get(command_name)
            command_handler = command_handler_cls(message)
            command_handler.handle(callback, value)
        else:
            # проверим что это команда
            if message.get('entities') and message['entities'][0]['type'] == 'bot_command':
                entity = message['entities'][0]
                offset, length = entity['offset'], entity['length']
                command_name = message['text'][offset: length + offset]
                command_handler_cls = commands_avalible.get(command_name)
                if command_handler_cls:
                    command_handler = command_handler_cls(message)
                else:
                    command_handler = commands_avalible['/start'](message)
                command_handler.answer()
            else:
                chat_id = message['chat']['id']
                last_command = redis_store.get('chat:command:%s' % chat_id)
                if last_command is None:
                    command_handler = commands_avalible['/start'](message)
                    command_handler.answer()
                else:
                    last_command = last_command.decode().split(":")
                    if len(last_command) == 1:
                        value = None
                    else:
                        value = last_command[1]
                    last_command = last_command[0]
                    handler = commands_avalible[last_command](message)
                    handler.handle_message(value)
        return '', 200


class OrderView(BaseView):
    methods = ['POST']

    def post(self):
        email = request.args.get('email', '') or request.form.get('email', '')

        match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)
        if match is None:
            pass
        return jsonify({'ok': False, 'error_code': 400, 'description': 'Invalid Email'})
        # проверка на одноразовость
        params = {'api': 'disposable', 'email': email}
        r = requests.get('https://api.debounce.io/v1/', params=params)
        r_data = r.json()
        if r_data['disposable'] == 'true':
            return jsonify({'ok': False, 'error_code': 400, 'description': 'Invalid Email'})
        q = db.session.query(Service).filter(Service.trial == True,
                                             Service.date_out == None,
                                             Service.email_order == email)

        if db.session.query(q.exists()).scalar():
            return jsonify({'ok': False, 'error_code': 402, 'description': 'Payment Required'})

        order_hash = generate_hex_string()
        redis_key = 'email:order:{}'.format(order_hash)
        redis_store.set(redis_key, email)
        # храним результат сутки
        redis_store.expire(redis_key, timedelta(days=1))
        ctx = {'url': url_for('v1.OrderConfirmView', _external=True) + \
                      '?order={}'.format(order_hash)}

        subject = _('Server Order')

        send_email(email, subject, ctx, template_name='email_order.html')
        return jsonify({'ok': True})


class OrderConfirmView(BaseView):
    methods = ['GET']

    def get(self):
        order_hash = request.args['order']
        redis_key = 'email:order:{}'.format(order_hash)
        email = redis_store.get(redis_key)
        if email is None:
            # неверная ссылка
            return render_template('order_invalid.html')
        redis_store.delete(redis_key)

        email = email.decode()

        # проверка не идет ли установка
        redis_key = 'installing:email:{}'.format(email)
        if redis_store.get(redis_key):
            return render_template('order_inprocess.html')
        redis_store.set(redis_key, '1')
        redis_store.expire(redis_key, 600)  # удаляется в task notify_user

        # задачи для создания
        test_period = current_app.config['FREE_TEST_DRIVE_MINS']
        tasks_list = list()
        query = find_server_for_install(email=email)
        locale = get_locale()
        if locale is None:
            locale = 'en'
        else:
            locale = 'ru' if locale.language == 'ru' else 'en'

        if query is None:
            # подходящий сервер не найден
            tasks_list.append(tasks.create_droplet.s())
            tasks_list.append(tasks.waiting_power_on_droplet.s(email_order=email, port=443))

            tasks_list.append(tasks.install_service.s(test_period=20))
        else:
            server_id, ip, _count = query
            query_ports = db.session.query(Service.port).filter_by(server_id=server_id, date_out=None).all()
            if query_ports:
                query_ports = {x[0] for x in query_ports}
            else:
                query_ports = set()
            # определим на какой порт ставить
            port = list(set(current_app.config['PORT_SERVER']) - query_ports)[0]

            service_obj = Service(
                server_id=server_id,
                email_order=email,
                ip=ip,
                port=port,
                trial=True,
            )
            db.session.add(service_obj)
            db.session.commit()

            tasks_list.append(tasks.install_service.si(service_obj.id, test_period))

        tasks_list.append(tasks.notify_user_email.s(email=email,
                                                    action='server_installed',
                                                    locale=locale,
                                                    redis_key=redis_key)
                          )
        chain(*tasks_list).apply_async()

        return render_template('order_success_confirm.html')


api_v1.add_url_rule('/botgram/', view_func=BotView.as_view('BotView'))
api_v1.add_url_rule('/order/', view_func=OrderView.as_view('OrderView'))
api_v1.add_url_rule('/order/confirm/', view_func=OrderConfirmView.as_view('OrderConfirmView'))
