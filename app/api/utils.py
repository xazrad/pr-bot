import binascii
import base64
import os
import urllib.parse as urlparse
from datetime import datetime
from urllib.parse import urlencode

import requests
from sqlalchemy import func, and_

from flask import current_app, render_template, render_template_string

from app.extensions import db
from app.models import Service, Server
from celery_app import tasks


def full_url_payment(service_id, payer, method='paypal'):
    type = payer['type']  # chat_id or email
    value = payer['value']

    if type == 'chat_id':
        custom_str = '0,{},{}'.format(value, service_id).encode()
    else:
        custom_str = '1,{},{}'.format(value, service_id).encode()

    custom = base64.urlsafe_b64encode(custom_str).decode()

    schema = current_app.config['PREFERRED_URL_SCHEME']
    server_name = current_app.config['SERVER_HOST']
    if method == 'paypal':
        url = '{}://{}/p/paypal/'.format(schema, server_name)

    if method == 'yandex':
        url = '{}://{}/p/yandex/'.format(schema, server_name)

    if method == 'crypto':
        url = '{}://{}/p/crypto/'.format(schema, server_name)

    params = {'q': custom}

    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)

    url_parts[4] = urlencode(query)
    return urlparse.urlunparse(url_parts)


def find_server_for_install(chat_id=None, email=None, port=None):
    # найдем серверы пользователя
    user_servers = db.session.query(Service).filter_by(date_out=None)

    if chat_id:
        user_servers = user_servers.filter_by(chat_id=chat_id)
    if email:
        user_servers = user_servers.filter_by(email_order=email)

    user_servers = user_servers.with_entities('server_id').distinct().all()

    if user_servers:
        user_servers = [x[0] for x in user_servers]

    # TODO: исключиим те сервера с котороых пользователь переезжал за сутки

    # исключим сервера где порт занят
    if port:
        excluded_servers = db.session.query(Service).filter_by(date_out=None,
                                                               port=port).with_entities('server_id').distinct().all()
        excluded_servers = [x[0] for x in excluded_servers]
        user_servers.extend(excluded_servers)
    # найдем подходящий не перезагруженный сервер
    query = db.session.query(
        Server.id,
        Server.ip,
        func.count(Service.id)
    )
    query = query.join(Service, and_(Service.server_id == Server.id, Service.date_out == None), isouter=True)
    query = query.filter(Server.status == 'done')
    query = query.filter(Server.date_out == None)
    query = query.filter(Server.is_blocked == False)
    query = query.group_by(Server.id)
    query = query.having(func.count(Service.id) < current_app.config['MAX_DOCKER_CONTAINER_SERVER'])

    # отфильтруем сервера уже имеющиеся у пользователя
    if user_servers:
        query = query.filter(~Server.id.in_(user_servers))
    return query.first()


def generate_hex_string():
    return binascii.b2a_hex(os.urandom(16)).decode()


def send_email(to, subject, context, template_name=None, template_sting=None, is_invoice=False):
    context['subject'] = subject
    if template_name:
        body = render_template(template_name, **context)
    else:
        body = render_template_string(template_sting, **context)
    fp = open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           'static', 'img', 'logo.png'), 'rb')

    files = [("inline", fp)]

    if is_invoice:
        fp_pp = open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'static', 'img', 'pp.gif'), 'rb')
        fp_cp = open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'static', 'img', 'cp.jpg'), 'rb')
        files.append(("inline", fp_pp))
        files.append(("inline", fp_cp))

    requests.post(
        current_app.config['MAILGUN_URL'],
        auth=("api", current_app.config['MAILGUN_KEY']),
        data={"from": "MTProxy Shop <noreply@mtproxy.shop>",
              "to": [to],
              "subject": subject,
              "html": body,},
        files=files
    )
    fp.close()
    if is_invoice:
        fp_pp.close()
        fp_cp.close()


def get_tasks_change_IP(session, service_obj, chat_obj, redis_key):
    # собираем задачи
    tasks_list = list()
    server_obj = find_server_for_install(chat_id=chat_obj.id, port=service_obj.port)
    if server_obj is None:
        # подходящий сервер не найден
        tasks_list.append(tasks.create_droplet.s())
        tasks_list.append(tasks.waiting_power_on_droplet.s(chat_id=chat_obj.id,
                                                           email_order=service_obj.email_order,
                                                           port=service_obj.port,
                                                           reinstall=service_obj.id,
                                                           auto_ip=service_obj.auto_ip))
        tasks_list.append(tasks.install_service.s(test_period=None, is_new=False, tag=service_obj.tag))
    else:
        server_id, ip, _count = server_obj
        service_obj_new = Service(
            server_id=server_id,
            chat_id=chat_obj.id,
            ip=ip,
            port=service_obj.port,
            email_order=service_obj.email_order,
            secret_key=service_obj.secret_key,
            prepaid=service_obj.prepaid,
            name=service_obj.name,
            tag=service_obj.tag,
            auto_ip=service_obj.auto_ip,
            trial=False,
        )
        session.add(service_obj_new)
        session.commit()

        tasks_list.append(tasks.install_service.si(service_obj_new.id, test_period=None,
                                                   is_new=False, tag=service_obj.tag))

    # обновить
    tasks_list.append(tasks.update_dns_name_amazon.s())

    tasks_list.append(tasks.notify_user.s(chat_id=chat_obj.id,
                                          action='server_installed',
                                          locale=chat_obj.locale,
                                          redis_key=redis_key
                                          ))
    tasks_list.append(tasks.destroy_service.si(service_obj.ip, service_obj.name))
    # проверим есть ли еще сервисы на сервере
    installed_services = session.query(Service).filter(
        Service.server_id == service_obj.server_id,
        Service.id != service_obj.id,
        Service.date_out == None
    ).count()
    if installed_services == 0:
        service_obj.server.date_out = datetime.utcnow()
        tasks_list.append(tasks.destroy_droplet.si(service_obj.server.droplet_id, service_obj.server.provider))
    service_obj.date_out = datetime.utcnow()
    session.commit()

    return tasks_list
