import base64
from datetime import datetime, timedelta

import telepot
from celery import chain
from emoji import emojize
from flask_babel import lazy_gettext as _
from flask_babel import refresh
from itsdangerous import TimestampSigner
from sqlalchemy import func

from flask import current_app, g

from app.extensions import amplitude_logger, db, redis_store
from app.models import Chat, Service
from celery_app import tasks

from .utils import get_tasks_change_IP, find_server_for_install, full_url_payment


signer = TimestampSigner(current_app.config['SECRET_KEY'])

commands_avalible = {}


class CommandMetaclass(type):

    def __new__(cls, name, bases, attrs):
        cls = super().__new__(cls, name, bases, attrs)
        parents = [b for b in bases if isinstance(b, CommandMetaclass)]
        if not parents:
            return cls
        commands_avalible[cls.command_name] = cls
        return cls


class BaseCommand(metaclass=CommandMetaclass):
    def __init__(self, message, text=None):
        self.message = message
        self.chat_id = message['chat']['id']
        if text is None:
            self.text = message.get('text')
        else:
            self.text = text
        self.bot = telepot.Bot(current_app.config['BOT_TOKEN'])
        self.redis_store_key = 'chat:command:%s' % self.chat_id
        redis_store.set(self.redis_store_key, self.command_name)

    def answer(self):
        if isinstance(self.answer_message, list):
            text = '\n'.join(map(lambda x: str(x), self.answer_message))
        else:
            text = self.answer_message
        try:
            self.bot.sendMessage(self.chat_id, text)
        except telepot.exception.BotWasBlockedError:
            pass

    def handle_message(self, *args, **kwargs):
        command_handler = commands_avalible['/start'](self.message)
        command_handler.answer()


class StartCommand(BaseCommand):
    command_name = '/start'
    answer_message = [_('Hello!'),
                      _('I will help you to create your own MTProxy server.'),
                      _('The server has free trial period of %s minutes.'),
                      _('The server cost is 3.99 USD per month.'),
                      ]

    def _check_payload(self):
        if not self.message.get('entities'):
            return

        if self.message['entities'][0]['type'] != 'bot_command':
            return

        payload = self.message['text'][len(self.command_name)+1:]

        if not payload:
            return

        prep_payload = payload.ljust(len(payload) + len(payload) % 4, '=')
        try:
            email, sevice_id = base64.urlsafe_b64decode(prep_payload).decode().split(',')
        except:
            if not g.chat.utm:
                g.chat.utm = payload[:24]
                db.session.commit()
            return

        q = db.session.query(Service).filter(
            Service.id == sevice_id,
            Service.email_order == email,
            Service.chat_id == None
        )

        if not db.session.query(q.exists()):
            return
        q.update({'chat_id': g.chat.id})
        db.session.commit()

    def answer(self):
        self._check_payload()
        flag = 'United_Kingdom' if g.chat.locale == 'ru' else 'Russia'
        alternative_console = 'en' if g.chat.locale == 'ru' else 'ru'
        buttons = [
            [
                {'text': '{}'.format(_('Create a new server')),
                 'callback_data': "{},create,".format(self.command_name, )}
            ],
            [
                {'text': '{}'.format(_('Server list')),
                 'callback_data': "{},list,".format(self.command_name)},
            ],
            [
                {'text': emojize(':{}: Lang/Язык'.format(flag)),
                 'callback_data': "{},language,{}".format(self.command_name, alternative_console)
                 },
                {'text': emojize(':SOS_button: {}'.format(_('Support'))),
                 'callback_data': "{},help,".format(self.command_name)
                 },
            ],
            [
                {'text': '{}'.format(_('About service')),
                 'url': 'https://mtproxy.shop/'}
            ],
            [
                {'text': '{}'.format(_('Referral program')),
                 'callback_data': "{},referral,".format(self.command_name)},
            ],
        ]
        text = '\n'.join(map(lambda x: str(x), self.answer_message))
        text = text % current_app.config['FREE_TEST_DRIVE_MINS']
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'create':
            cl = commands_avalible['/create'](self.message)
            cl.answer()

        if callback == 'list':
            cl = commands_avalible['/list'](self.message)
            cl.answer()

        if callback == 'language':
            cl = commands_avalible['/language'](self.message)
            cl.answer()

        if callback == 'help':
            cl = commands_avalible['/help'](self.message)
            cl.answer()

        if callback == 'referral':
            cl = commands_avalible['/referral'](self.message)
            cl.answer()


class ServerListCommand(BaseCommand):
    command_name = '/list'

    def answer(self):
        service_objs = db.session.query(Service).filter_by(chat_id=g.chat.id, status='done', date_out=None).all()
        if len(service_objs) == 0:
            # проверка что установка не в процессе
            redis_key = 'change_ip:chat:{}'.format(g.chat.id)
            if redis_store.get(redis_key):
                answer_message = [
                    _('The process of changing IP is in progress.'),
                ]
                text = '\n'.join(map(lambda x: str(x), answer_message))
                try:
                    self.bot.sendMessage(self.chat_id, text)
                except telepot.exception.BotWasBlockedError:
                    pass
                return
            text = [_('You have not created any servers yet.')]
            buttons = [
                [{'text': '{}'.format(_('Create now')), 'callback_data': "{},create,".format(self.command_name)}]
            ]

        else:
            text = [_('List of all servers you have created:')]
            buttons = []
            for service_obj in service_objs:
                buttons.append(
                    [{'text': '{}'.format(service_obj.name),
                      'callback_data': "{},server,{}".format(self.command_name, service_obj.id)}]
                )

        buttons.append(
            [{'text': emojize(':house: {}'.format(_('Main menu'))),
              'callback_data': "{},home,".format(self.command_name)}],
        )

        text = '\n'.join(map(lambda x: str(x), text))
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'home':
            cl = commands_avalible['/start'](self.message)
            cl.answer()

        if callback == 'create':
            cl = commands_avalible['/create'](self.message)
            cl.answer()

        if callback == 'server':
            cl = commands_avalible['/server'](self.message)
            cl.answer(value)


class ServerViewCommand(BaseCommand):
    command_name = '/server'

    def answer(self, value):
        service_obj = db.session.query(Service).filter_by(id=value, chat_id=g.chat.id,
                                                          date_out=None, status='done').first()
        if service_obj is None:
            text = [_('Server deleted or not yet created.')]
            buttons = [
                [
                    {'text': '{}'.format(_('<< Back')),
                     'callback_data': "{},back,".format(self.command_name, )}
                ],
            ]
        else:
            secret_key_client = service_obj.secret_key
            secret_key_client = 'dd%s' % secret_key_client
            url_connect_ip = "https://t.me/proxy?server={}&port={}&secret={}".format(service_obj.ip,
                                                                                     service_obj.port,
                                                                                     secret_key_client)
            text = ['IP: ```{}```'.format(service_obj.ip),
                    'Port: ```{}```'.format(service_obj.port),
                    'Secret key: ```{}```'.format(service_obj.secret_key),
                    '{}: {} UTC'.format(_('Created'), service_obj.date_in.strftime('%Y-%m-%d %H:%M:%S')),
                    '{}: {} UTC'.format(_('Paid until'), service_obj.prepaid.strftime('%Y-%m-%d %H:%M:%S')),
                    _('Link with IP'),
                    url_connect_ip
                    ]
            buttons = [
                [{'text': '{} IP'.format(_('Connection link')),
                  'url': url_connect_ip}],
                [{'text': '{}'.format(_('Edit channel tag')),
                  'callback_data': "{},tag_add,{}".format(self.command_name, service_obj.id)}]
            ]
            # есть доменное имя
            name = '{}.{}'.format(service_obj.name, current_app.config['ROUTE53_DOMAIN'])
            url_connect_domain = "https://t.me/proxy?server={}&port={}&secret={}".format(name,
                                                                                         service_obj.port,
                                                                                         secret_key_client)
            text.extend([
                _('Link with domain (recommended)'),
                url_connect_domain
            ])

            buttons.insert(1, [{'text': '{} Domain'.format(_('Connection link')), 'url': url_connect_domain}])

            if service_obj.tag:
                buttons[-1].append({'text': '{}'.format(_('Delete channel tag')),
                                    'callback_data': "{},tag_remove,{}".format(self.command_name, service_obj.id)})
            buttons.extend([
                [
                    {'text': '{}'.format(_('Change IP')),
                     'callback_data': "{},change_ip,{}".format(self.command_name, service_obj.id)}
                ],
                [
                    {'text': '{}'.format(_('Is IP Blocked In Russia? (beta)')),
                     'callback_data': "{},check_rkn,{}".format(self.command_name, service_obj.id)}
                ],
                [
                    {'text': '{}'.format(_('Auto change IP')),
                     'callback_data': "{},auto_change_ip,{}".format(self.command_name, service_obj.id)}
                ],

                [
                    {'text': '{}'.format(_('Renew server rent for next 30 days')),
                     'callback_data': "{},payment,{}".format(self.command_name, service_obj.id)}
                ],
                [
                    {'text': '{}'.format(_('<< Back')), 'callback_data': "{},back,".format(self.command_name, )}
                ],
            ])

        text = '\n'.join(map(lambda x: str(x), text))
        try:
            self.bot.sendMessage(self.chat_id, text, parse_mode="Markdown", reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'back':
            cl = commands_avalible['/list'](self.message)
            cl.answer()

        if callback == 'tag_remove':
            cl = commands_avalible['/tag_remove'](self.message)
            cl.answer(value)

        if callback == 'tag_add':
            cl = commands_avalible['/tag_add'](self.message)
            cl.answer(value)

        if callback == 'payment':
            cl = commands_avalible['/payment'](self.message)
            cl.answer(value)

        if callback == 'check_rkn':
            service_obj = db.session.query(Service).get(value)
            tasks_list = list()
            tasks_list.append(tasks.check_service_agent.si(value, service_obj.ip))
            tasks_list.append(tasks.notify_user.s(chat_id=g.chat.id,
                                                  action='check_rkn',
                                                  locale=g.chat.locale,
                                                  redis_key=None
                                                  ))
            chain(*tasks_list).apply_async(link_error=tasks.notify_user_error.si(
                chat_id=g.chat.id,
                locale=g.chat.locale,
                redis_key=None)
            )

        if callback == 'change_ip':
            cl = commands_avalible['/change_ip'](self.message)
            cl.answer(value)

        if callback == 'auto_change_ip':
            cl = commands_avalible['/auto_change_ip'](self.message)
            cl.answer(value)


class CreateCommand(BaseCommand):
    command_name = '/create'
    answer_message = [_('A new server creation will take approximately 2 minutes.'),
                      _('You are given a free trial period of %s minutes.'),
                      _('We will send you the data for connection and invoice for payment.'),
                      _('If there is no payment during the free trial period, the server will be deleted.'),
                      ]

    def answer(self):
        buttons = [
            [{'text': '{}'.format(_('Create now')), 'callback_data': "{},create,".format(self.command_name)}],
            [{'text': emojize(':house: {}'.format(_('Main menu'))), 'callback_data': "{},home,".format(self.command_name)}],
        ]

        text = '\n'.join(map(lambda x: str(x), self.answer_message))
        text = text % current_app.config['FREE_TEST_DRIVE_MINS']
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'home':
            cl = commands_avalible['/start'](self.message)
            cl.answer()
        if callback == 'create':
            # создание нового сервиса
            buttons = [
                [
                    {'text': emojize(':house: {}'.format(_('Main menu'))),
                     'callback_data': "{},home,".format(self.command_name)}
                ],
            ]
            # проверка что установка не в процессе
            redis_key = 'installing:chat:{}'.format(g.chat.id)
            if redis_store.get(redis_key):
                answer_message = [
                    _('The process of installing a new server is in progress.'),
                ]
                text = '\n'.join(map(lambda x: str(x), answer_message))
                try:
                    self.bot.sendMessage(self.chat_id, text, reply_markup={
                        'inline_keyboard': buttons
                    })
                except telepot.exception.BotWasBlockedError:
                    pass
                return

            # проверка что активных триалов
            q = db.session.query(Service).filter_by(chat_id=g.chat.id, trial=True, date_out=None)
            if db.session.query(q.exists()).scalar():
                answer_message = [
                    _('You are using a trial period.'),
                    _('To create a new server, pay for the previous trial server.')
                ]
                text = '\n'.join(map(lambda x: str(x), answer_message))
                try:
                    self.bot.sendMessage(self.chat_id, text, reply_markup={
                        'inline_keyboard': buttons
                    })
                except telepot.exception.BotWasBlockedError:
                    pass
                return

            # проверка что пользователь не злоупотребляет
            count, min_date = db.session.query(
                func.count(Service.id),
                func.min(Service.date_in)
            ).filter(
                Service.chat_id == g.chat.id,
                Service.trial == True,
                Service.date_in > datetime.utcnow() - timedelta(days=14)
            ).all()[0]

            if count > 4:
                min_date = min_date + timedelta(days=30)
                min_date = min_date.strftime("%Y-%m-%d %H:%M:%S")
                answer_message = [
                    _('Sorry.'),
                    _('Number of trials has been exceeded.'),
                    _('We will limit the creation of trial servers for you until %s UTC.')
                ]
                text = '\n'.join(map(lambda x: str(x), answer_message))
                text = text % min_date
                try:
                    self.bot.sendMessage(self.chat_id, text, reply_markup={
                        'inline_keyboard': buttons
                    })
                except telepot.exception.BotWasBlockedError:
                    pass
                return

            tasks_list = list()
            test_period = current_app.config['FREE_TEST_DRIVE_MINS']
            redis_store.set(redis_key, '1')
            redis_store.expire(redis_key, 600)  # удаляется в task notify_user

            query = find_server_for_install(chat_id=g.chat.id)
            if query is None:
                # подходящий сервер не найден
                tasks_list.append(tasks.create_droplet.s())
                tasks_list.append(tasks.waiting_power_on_droplet.s(chat_id=g.chat.id, port=443))
                tasks_list.append(tasks.install_service.s(test_period=test_period))
            else:
                server_id, ip, _count = query
                query_ports = db.session.query(Service.port).filter_by(server_id=server_id, date_out=None).all()
                if query_ports:
                    query_ports = {x[0] for x in query_ports}
                else:
                    query_ports = set()
                # определим на какой порт ставить
                port = list(set(current_app.config['PORT_SERVER']) - query_ports)[0]

                service_obj = Service(
                    server_id=server_id,
                    chat_id=g.chat.id,
                    ip=ip,
                    port=port,
                    trial=True,
                )
                db.session.add(service_obj)
                db.session.commit()

                tasks_list.append(tasks.install_service.si(service_obj.id, test_period))

            tasks_list.append(tasks.set_dns_name_amazon.s())
            tasks_list.append(tasks.notify_user.s(chat_id=g.chat.id,
                                                  action='server_installed',
                                                  locale=g.chat.locale,
                                                  redis_key=redis_key
                                                  ))
            chain(*tasks_list).apply_async(link_error=tasks.notify_user_error.si(chat_id=g.chat.id,
                                                                                 locale=g.chat.locale,
                                                                                 redis_key=redis_key))
            answer_message = [
                _('The task is queued. Please wait.'),
            ]
            text = '\n'.join(map(lambda x: str(x), answer_message))

            try:
                self.bot.sendMessage(self.chat_id, text, reply_markup={
                    'inline_keyboard': buttons
                })
            except telepot.exception.BotWasBlockedError:
                pass
            # создадим информацию amtidude
            event_args = {"user_id": g.chat.id,
                          "event_type": "trial_start",
                          "event_properties": {"period": test_period}}
            amplitude_logger.create_event(**event_args)


class LanguageCommand(BaseCommand):
    command_name = '/language'

    def answer(self):
        buttons = [
            [{'text': emojize(':United_Kingdom: English'), 'callback_data': "{},language,eng".format(self.command_name)}],
            [{'text': emojize(':Russia: Russian'), 'callback_data': "{},language,ru".format(self.command_name)}]
        ]
        text = (
            "Choose a language:\n"
            "Выберите язык:\n"
        )
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        # измения язык в настройках
        g.chat.locale = value
        db.session.commit()
        refresh()
        cl = commands_avalible['/start'](self.message)
        cl.answer()


class SupportCommand(BaseCommand):
    command_name = '/help'
    answer_message = [_('Do you have any questions?'),
                      _('Contact Us @setproxyteam'),
                      ]

    def answer(self):
        buttons = [
            [
                {'text': emojize(':house: {}'.format(_('Main menu'))),
                 'callback_data': "{},,".format(self.command_name)}
            ],
        ]

        text = '\n'.join(map(lambda x: str(x), self.answer_message))
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, *args, **kwargs):
        cl = commands_avalible['/start'](self.message)
        cl.answer()


class TagRemoveCommand(BaseCommand):
    command_name = '/tag_remove'

    def answer(self, value):
        service_obj = db.session.query(Service).filter_by(id=value, chat_id=g.chat.id,
                                                          status='done', date_out=None).first()
        buttons = []
        if service_obj is None:
            text = [
                _('Server deleted or not yet created.')
            ]
        else:
            text = [
                _('Delete the tag for the server?'),
                'Server: {}:{}'.format(service_obj.ip, service_obj.port),
                _('Are you sure?')
            ]
            buttons.append([
                {'text': '{}'.format(_('Yes! I am sure!')),
                 'callback_data': "{},sure,{}".format(self.command_name, service_obj.id)}
            ])

        buttons.append([
            {'text': '{}'.format(_('<< Back')),
             'callback_data': "{},back,{}".format(self.command_name, service_obj.id)}
        ])

        text = '\n'.join(map(lambda x: str(x), text))
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'back':
            cl = commands_avalible['/server'](self.message)
            cl.answer(value)

        if callback == 'list':
            cl = commands_avalible['/list'](self.message)
            cl.answer()

        if callback == 'sure':
            service_obj = db.session.query(Service).filter_by(id=value, chat_id=g.chat.id,
                                                              date_out=None, status='done').first()
            if service_obj is None:
                text = '{}'.format(_('Server deleted or not yet created.'))
            else:
                text = '{}'.format(_('The task is queued. Please wait.'))
                # отложенные задачи
                from celery_app.tasks import change_tag, notify_user, notify_user_error

                tasks_list = list()
                tasks_list.append(change_tag.s(service_id=value))
                tasks_list.append(notify_user.si({'ip': service_obj.ip,
                                                 'port': service_obj.port},
                                                chat_id=g.chat.id,
                                                action='tag_removed',
                                                locale=g.chat.locale))
                chain(*tasks_list).apply_async(link_error=notify_user_error.si(chat_id=g.chat.id, locale=g.chat.locale))
            buttons = [
                [{'text': '{}'.format(_('<< Back to server list')),
                  'callback_data': "{},list,".format(self.command_name)}]
            ]
            try:
                self.bot.sendMessage(self.chat_id, text, reply_markup={
                    'inline_keyboard': buttons
                })
            except telepot.exception.BotWasBlockedError:
                pass


class TagAddCommand(BaseCommand):
    command_name = '/tag_add'

    def answer(self, value):
        service_obj = db.session.query(Service).filter_by(id=value, chat_id=g.chat.id,
                                                          status='done', date_out=None).first()
        buttons = []
        if service_obj is None:
            text = [
                _('Server deleted or not yet created.')
            ]
            buttons.append([
                {'text': '{}'.format(_('<< Back')),
                 'callback_data': "{},list,".format(self.command_name)}
            ])

        else:
            text = [
                _('The tag is managed through a bot @MTProxybot.'),
                _('Step by step:'),
                _('1. Open @MTProxyBot and send the command */newproxy*'),
                _('2. Send a message to bot'),
                '```%s:%s```' % (service_obj.ip, service_obj.port),
                _('3. Then send the secret key to the bot'),
                '```%s```' % service_obj.secret_key,
                _('4. In response, you will receive a message, which you need to forward to this bot')
            ]
            # установим значение в редис
            redis_store.set(self.redis_store_key, '{}:{}'.format(self.command_name, value))

            buttons.append([
                {'text': '{}'.format(_('<< Back')),
                 'callback_data': "{},back,{}".format(self.command_name, service_obj.id)}
            ])

        text = '\n'.join(map(lambda x: str(x), text))
        try:
            self.bot.sendMessage(self.chat_id, text, parse_mode="Markdown", reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'back':
            cl = commands_avalible['/server'](self.message)
            cl.answer(value)

        if callback == 'list':
            cl = commands_avalible['/list'](self.message)
            cl.answer()

    def handle_message(self, value):
        '''
        self.text - значение присланное
        :param value: service_id
        :return:
        '''
        # установим значение в редис
        redis_store.set(self.redis_store_key, '{}:{}'.format(self.command_name, value))
        text_error = '{} {}'.format(_('Sorry! Invalid value!'), _('Try it again!'))
        if self.text is None:
            return 
        if len(self.text) != 32:
            try:
                self.bot.sendMessage(self.chat_id, text_error)
            except telepot.exception.BotWasBlockedError:
                pass
            return
        try:
            int(self.text, 16)
        except ValueError:
            try:
                self.bot.sendMessage(self.chat_id, text_error)
            except telepot.exception.BotWasBlockedError:
                pass
            return

        redis_store.delete(self.redis_store_key)
        service_obj = db.session.query(Service).filter_by(id=value, chat_id=g.chat.id,
                                                          status='done', date_out=None).first()
        if service_obj is None:
            text = '{}'.format(_('Server deleted or not yet created.'))
            buttons = [
                [{'text': '{}'.format(_('<< Back to server list')),
                  'callback_data': "{},list,".format(self.command_name)}]
            ]

        else:
            text = '{}'.format(_('The task is queued. Please wait.'))
            buttons = [
                [{'text': '{}'.format(_('<< Back')),
                  'callback_data': "{},back,{}".format(self.command_name, value)}]
            ]
            # отложенные задачи
            from celery_app.tasks import change_tag, notify_user, notify_user_error

            tasks_list = list()
            tasks_list.append(change_tag.s(service_id=value, tag=self.text))
            tasks_list.append(notify_user.si({'ip': service_obj.ip,
                                              'port': service_obj.port},
                                             chat_id=g.chat.id,
                                             action='tag_established',
                                             locale=g.chat.locale))
            chain(*tasks_list).apply_async(link_error=notify_user_error.si(chat_id=g.chat.id, locale=g.chat.locale))
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass


class PaymentCommand(BaseCommand):
    command_name = '/payment'

    def answer(self, value):
        service_obj = db.session.query(Service).filter_by(id=value, chat_id=g.chat.id,
                                                          status='done', date_out=None).first()
        buttons = []
        if service_obj is None:
            text = [
                _('Server deleted or not yet created.')
            ]
            buttons.append([
                {'text': '{}'.format(_('<< Back to server list')),
                 'callback_data': "{},list,".format(self.command_name)}
            ])

        else:
            text = [
                _('Choose a payment method.'),
            ]

            buttons.extend([
                [
                    {'text': 'PayPal, Credit cards',
                     'url': full_url_payment(service_obj.id, payer={'type': 'chat_id', 'value': g.chat.id}, method='paypal')}
                ],
                [
                    {'text': 'Yandex Money',
                     'url': full_url_payment(service_obj.id, payer={'type': 'chat_id', 'value': g.chat.id}, method='yandex')}
                ],
                [
                    {'text': '{}'.format(_('Crypto currency')),
                     'url': full_url_payment(service_obj.id, payer={'type': 'chat_id', 'value': g.chat.id}, method='crypto')}
                ],
                [
                    {'text': '{}'.format(_('<< Back')),
                     'callback_data': "{},back,{}".format(self.command_name, service_obj.id)}
                ]
            ])

        text = '\n'.join(map(lambda x: str(x), text))
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'back':
            cl = commands_avalible['/server'](self.message)
            cl.answer(value)

        if callback == 'list':
            cl = commands_avalible['/list'](self.message)
            cl.answer()


class ChangeIP(BaseCommand):
    command_name = '/change_ip'

    def answer(self, value):
        service_obj = db.session.query(Service).filter_by(id=value,
                                                          chat_id=g.chat.id,
                                                          status='done',
                                                          date_out=None).first()
        buttons = []
        if service_obj is None:
            text = [
                _('Server deleted or not yet created.')
            ]
            buttons.append([
                {'text': '{}'.format(_('<< Back')),
                 'callback_data': "{},list,".format(self.command_name)}
            ])
        else:
            text = [
                _('You can change the server`s IP.'),
                _('The procedure will take about 10 minutes.'),
                _('If you had a tied domain name, the changes will take effect in 5 min.'),
                _('The service is available only for paid servers.'),
            ]

            if not service_obj.trial:
                buttons.append([
                        {'text': '{}'.format(_('Confirm')),
                         'callback_data': "{},confirm,{}".format(self.command_name, service_obj.id)}
                ])
            buttons.extend([
                [
                    {'text': '{}'.format(_('<< Back')),
                     'callback_data': "{},back,{}".format(self.command_name, service_obj.id)}
                ]
            ])
        text = '\n'.join(map(lambda x: str(x), text))
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'back':
            cl = commands_avalible['/server'](self.message)
            cl.answer(value)

        if callback == 'list':
            cl = commands_avalible['/list'](self.message)
            cl.answer()

        if callback == 'confirm':
            service_obj = db.session.query(Service).filter_by(id=value,
                                                              chat_id=g.chat.id,
                                                              status='done',
                                                              date_out=None).first()
            buttons = []
            if service_obj is None:
                text = [
                    _('Server deleted or not yet created.')
                ]
                buttons.append([
                    {'text': '{}'.format(_('<< Back')),
                     'callback_data': "{},list,".format(self.command_name)}
                ])
                try:
                    self.bot.sendMessage(self.chat_id, text, reply_markup={
                        'inline_keyboard': buttons
                    })
                except telepot.exception.BotWasBlockedError:
                    pass
                return
            buttons = [
                [{'text': '{}'.format(_('<< Back')),
                  'callback_data': "{},back,{}".format(self.command_name, value)}]
            ]
            # проверка что установка не в процессе
            redis_key = 'change_ip:chat:{}'.format(g.chat.id)
            if redis_store.get(redis_key):
                answer_message = [
                    _('The process of changing IP is in progress.'),
                ]
                text = '\n'.join(map(lambda x: str(x), answer_message))
                try:
                    self.bot.sendMessage(self.chat_id, text, reply_markup={
                        'inline_keyboard': buttons
                    })
                except telepot.exception.BotWasBlockedError:
                    pass
                return
            # собираем задачи
            tasks_list = get_tasks_change_IP(db.session, service_obj, g.chat, redis_key)
            chain(*tasks_list).apply_async(link_error=tasks.notify_user_error.si(chat_id=g.chat.id,
                                                                                 locale=g.chat.locale,
                                                                                 redis_key=redis_key))
            answer_message = [
                _('The task is queued. Please wait.'),
            ]
            text = '\n'.join(map(lambda x: str(x), answer_message))
            try:
                self.bot.sendMessage(self.chat_id, text)
            except telepot.exception.BotWasBlockedError:
                pass


class Referral(BaseCommand):
    command_name = '/referral'
    answer_message = [
        "Для получения доступа к правилам и статистики реферальной программы пройдите по ссылке указанной ниже.",
        "Ссылка действительна 30 секунд.",
    ]

    def answer(self):
        buttons = [
            [
                {'text': "Обновить ссылку",
                 'callback_data': "{},referral,".format(self.command_name)}
            ],
            [
                {'text': emojize(':house: {}'.format(_('Main menu'))),
                 'callback_data': "{},main,".format(self.command_name)}
            ],
        ]
        schema = current_app.config['PREFERRED_URL_SCHEME']
        server_name = current_app.config['SERVER_HOST']
        ref_s = signer.sign(str(g.chat.id)).decode()

        url_ref = '{}://{}/ref/sign/{}/'.format(schema, server_name, ref_s)
        answer_message = self.answer_message.copy()
        answer_message.append(
            url_ref
        )
        text = '\n'.join(map(lambda x: str(x), answer_message))
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'main':
            cl = commands_avalible['/start'](self.message)
            cl.answer()

        if callback == 'referral':
            cl = commands_avalible['/referral'](self.message)
            cl.answer()


class AutoIPChange(BaseCommand):
    command_name = '/auto_change_ip'

    def answer(self, value):
        service_obj = db.session.query(Service).filter_by(id=value,
                                                          chat_id=g.chat.id,
                                                          status='done',
                                                          date_out=None).first()
        buttons = []
        if service_obj is None:
            text = [
                _('Server deleted or not yet created.')
            ]
            buttons.append([
                {'text': '{}'.format(_('<< Back')),
                 'callback_data': "{},list,".format(self.command_name)}
            ])
        else:
            text = [
                _('If your server is not available in Russia, we will automatically change IP.'),
                _('The service is available only for paid servers.'),
                _('Server: %s') % service_obj.name,
            ]

            if service_obj.auto_ip:
                auto_s = _('Auto change IP is enabled')
                auto_c = _('Disabled auto change IP')
            else:
                auto_s = _('Auto change IP is disabled')
                auto_c = _('Enable auto change IP')
            text.append(auto_s)

            if not service_obj.trial:
                buttons.append([
                        {'text': '{}'.format(auto_c),
                         'callback_data': "{},confirm,{}".format(self.command_name, service_obj.id)}
                ])
            buttons.extend([
                [
                    {'text': '{}'.format(_('<< Back')),
                     'callback_data': "{},back,{}".format(self.command_name, service_obj.id)}
                ]
            ])
        text = '\n'.join(map(lambda x: str(x), text))
        try:
            self.bot.sendMessage(self.chat_id, text, reply_markup={
                'inline_keyboard': buttons
            })
        except telepot.exception.BotWasBlockedError:
            pass

    def handle(self, callback, value):
        if callback == 'back':
            cl = commands_avalible['/server'](self.message)
            cl.answer(value)

        if callback == 'list':
            cl = commands_avalible['/list'](self.message)
            cl.answer()

        if callback == 'confirm':
            service_obj = db.session.query(Service).filter_by(id=value,
                                                              chat_id=g.chat.id,
                                                              status='done',
                                                              date_out=None).first()
            buttons = []
            if service_obj is None:
                text = [
                    _('Server deleted or not yet created.')
                ]
                buttons.append([
                    {'text': '{}'.format(_('<< Back')),
                     'callback_data': "{},list,".format(self.command_name)}
                ])

                text = '\n'.join(map(lambda x: str(x), text))

                try:
                    self.bot.sendMessage(self.chat_id, text, reply_markup={
                        'inline_keyboard': buttons
                    })
                except telepot.exception.BotWasBlockedError:
                    pass
                return

            buttons = [
                [{'text': '{}'.format(_('<< Back')),
                  'callback_data': "{},back,{}".format(self.command_name, value)}]
            ]

            # проставляем значение
            service_obj.auto_ip = not service_obj.auto_ip
            db.session.commit()

            if service_obj.auto_ip:
                text = [
                    _('Server: %s') % service_obj.name,
                    _('Auto change IP is enabled'),
                ]
            else:
                text = [
                    _('Auto change IP is disabled'),
                ]

            text = '\n'.join(map(lambda x: str(x), text))
            try:
                self.bot.sendMessage(self.chat_id, text, reply_markup={
                        'inline_keyboard': buttons
                    })
            except telepot.exception.BotWasBlockedError:
                pass



