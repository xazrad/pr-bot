from datetime import datetime

from flask_babel import refresh
from sqlalchemy.orm.exc import NoResultFound

from flask import current_app, g, request

from app.extensions import amplitude_logger, db
from app.models import Chat, User


def write_user_info(f):
    def wrap(*args, **kwargs):
        data = request.get_json(force=True)
        current_app.logger.warning('%s -- %s', request.path, request.data.decode())
        if data.get('edited_message'):
            return '', 200

        if data.get('callback_query'):
            message = data['callback_query']['message']
            callback_query = data['callback_query']['data']
        else:
            callback_query = None
            try:
                message = data['message']
            except KeyError:
                return '', 200
        user_data = message.get('from')

        locale = 'en'
        if user_data:
            q = User.query.filter_by(id=user_data['id'])
            if not db.session.query(q.exists()).scalar():
                user_obj = User(
                    id=user_data['id'],
                    is_bot=user_data['is_bot'],
                    first_name=user_data['first_name'],
                    last_name=user_data.get('last_name'),
                    username=user_data.get('username'),
                    language_code=user_data.get('language_code'),
                )
                db.session.add(user_obj)
                locale = 'ru' if 'ru' in user_data.get('language_code', '') else 'en'

        chat_data = message['chat']
        chat_id = chat_data['id']
        if chat_id < 0:
            return '', 200
        try:
            chat_inst = Chat.query.filter_by(id=chat_id).one()
        except NoResultFound:
            chat_inst = Chat(
                id=chat_id,
                type=chat_data['type'],
                title=chat_data.get('title'),
                username=chat_data.get('username'),
                first_name=chat_data.get('first_name'),
                last_name=chat_data.get('last_name'),
                locale=locale
            )
            db.session.add(chat_inst)
            # создадим информацию amtidude
            event_args = {"user_id": chat_id, "event_type": "start",
                          "event_properties": {"payload": message.get('text', '')},
                          "user_properties": {"username": message["chat"].get("username", ""),
                                              "first_name": message["chat"].get("first_name", ""),
                                              "last_name": message["chat"].get("last_name", ""),
                                              "language_code": message['from'].get('language_code', "")}}
            amplitude_logger.create_event(**event_args)
        chat_inst.last_active = datetime.utcnow()
        db.session.commit()
        g.chat = chat_inst
        from flask_babel import lazy_gettext as _
        refresh()
        '{}'.format(_('Hello!'))
        return f(*args, **kwargs, message=message, callback_query=callback_query)
    return wrap
