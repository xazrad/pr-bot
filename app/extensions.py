import json
import time

import requests
from flask import _app_ctx_stack
from flask_babel import Babel
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_redis import FlaskRedis
from raven.contrib.flask import Sentry


class AmplitudeFlask:

    def __init__(self, app=None, api_key=None, api_uri='https://api.amplitude.com/httpapi'):
        self.api_key = api_key
        self.api_uri = api_uri
        self.app = None
        if app is not None:
            self.init_app(app)

    def init_app(self, app, **kwargs):
        self.api_key = self.api_key or app.config.get('AMPLITUDE_API_KEY')
        if not hasattr(app, 'extensions'):
            app.extensions = {}
        app.extensions['amplitude'] = self
        self.app = app
        app.teardown_appcontext(self.teardown)

    def teardown(self, exception):
        ctx = _app_ctx_stack.top
        if ctx is None:
            return
        if exception is None and hasattr(ctx, 'event_packages'):
            self._log_event(ctx.event_packages)

    def create_event(self, **kwargs):
        ctx = _app_ctx_stack.top
        if ctx is None:
            return
        if not hasattr(ctx, 'event_packages'):
            ctx.event_packages = []

        event = {}
        user_id = kwargs['user_id']
        event_type = kwargs['event_type']
        event["user_id"] = str(user_id)
        event["event_type"] = event_type

        # integer epoch time in milliseconds
        event["time"] = int(time.time() * 1000)

        event_properties = kwargs.get('event_properties', None)
        if event_properties is not None and type(event_properties) == dict:
            event["event_properties"] = event_properties

        user_properties = kwargs.get('user_properties', None)
        if user_properties is not None and type(user_properties) == dict:
            event["user_properties"] = user_properties

        # ++ many other properties
        # details: https://amplitude.zendesk.com/hc/en-us/articles/204771828-HTTP-API
        ctx.event_packages.append(event)

    def _log_event(self, event_packages):
        if self.api_key and event_packages:
            event_package = [
                ('api_key', self.api_key),
                ('event', json.dumps(event_packages)),
            ]
            requests.post(self.api_uri, data=event_package)


amplitude_logger = AmplitudeFlask()

db = SQLAlchemy()

migrate = Migrate()

sentry = Sentry(dsn='https://fbc7b738b6fc47eeaaecaf0eaea2e4eb:14a303f91b8c47f1a39d35721549a4f8@sentry.io/1227282')

redis_store = FlaskRedis()

babel = Babel()