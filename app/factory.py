import os

from pathlib import Path

from flask import Flask, g, request

from .loggers import LoggingConfig

from .extensions import amplitude_logger, babel, db, migrate, redis_store, sentry


def create_app(register_blueprints=True):
    app = Flask(__name__)
    app.config.from_object(os.environ.get('SETTINGS'))

    babel.init_app(app)
    db.init_app(app)
    db.app = app
    redis_store.init_app(app)
    migrate.init_app(app, db)
    if not app.config['DEBUG']:
        sentry.init_app(app)
        amplitude_logger.init_app(app)

    # Register the blueprints
    if register_blueprints:
        log_config = LoggingConfig(
            Path(Path(app.root_path).parent.parent, 'log', 'bot.log'),
            Path(Path(app.root_path).parent.parent, 'log', 'bot.error')
        )
        for handler in log_config.handlers:
            app.logger.addHandler(handler)

        from app.api import api_v1
        from app.payment import payment_bp
        from app.referral import referral_bp

        app.register_blueprint(api_v1, url_prefix='/v1')
        app.register_blueprint(payment_bp, url_prefix='/p')
        app.register_blueprint(referral_bp, url_prefix='/ref')

        @babel.localeselector
        def get_locale():
            if getattr(g, 'chat', None):
                return g.chat.locale
            try:
                return request.accept_languages.best_match(['en', 'ru'])
            except:
                return 'ru'
    return app
