from datetime import datetime

from sqlalchemy.orm import object_session

from app.extensions import db


class User(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=False)
    is_bot = db.Column(db.BOOLEAN, nullable=False, default=False)
    first_name = db.Column(db.String(128), nullable=False)
    last_name = db.Column(db.String(128))
    username = db.Column(db.String(128))
    language_code = db.Column(db.String(16))

    def __repr__(self):
        return '<User %r>' % self.id


class Chat(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=False)
    type = db.Column(db.String(80), nullable=False)
    title = db.Column(db.String(80))
    username = db.Column(db.String(80))
    first_name = db.Column(db.String(128))
    last_name = db.Column(db.String(128))
    locale = db.Column(db.String(16))
    utm = db.Column(db.String(24), index=True)
    last_active = db.Column(db.DateTime, default=datetime.utcnow)

    @classmethod
    def after_insert(cls, mapper, connection, chat):
        # создаем реферальный аккуант
        object_session(chat).add(ReferralAccount(id=chat.id, utm=chat.id))

    def __repr__(self):
        return '<Chat %r>' % self.id


class Server(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    provider = db.Column(db.String(80), nullable=False, default='Vultr')
    droplet_id = db.Column(db.String(80), nullable=False)
    ip = db.Column(db.String(43), nullable=False)
    data_center = db.Column(db.String(64), nullable=False)
    name = db.Column(db.String(128), nullable=False)
    date_in = db.Column(db.DateTime, default=datetime.utcnow)
    date_out = db.Column(db.DateTime)
    status = db.Column(db.String(64), nullable=False, default='installing')
    is_blocked = db.Column(db.BOOLEAN, nullable=False, default=False)
    checked = db.Column(db.DateTime)

    def __repr__(self):
        return '<Server %r>' % self.id


class Service(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    server_id = db.Column(db.ForeignKey('server.id'), nullable=False, index=True)
    chat_id = db.Column(db.ForeignKey('chat.id'), index=True)
    email_order = db.Column(db.String(64), index=True)
    secret_key = db.Column(db.String(64))
    ip = db.Column(db.String(43), nullable=False)
    port = db.Column(db.INTEGER)
    trial = db.Column(db.Boolean)
    tag = db.Column(db.String(64))
    date_in = db.Column(db.DateTime, default=datetime.utcnow)
    date_out = db.Column(db.DateTime)
    status = db.Column(db.String(64), nullable=False, default='installing')
    name = db.Column(db.String(128))
    prepaid = db.Column(db.DateTime)
    commit = db.Column(db.String(64))
    auto_ip = db.Column(db.Boolean, default=False)

    server = db.relationship('Server', backref=db.backref('services', lazy='dynamic'))
    chat = db.relationship('Chat', backref=db.backref('services', lazy='dynamic'))

    def __repr__(self):
        return '<Service %r>' % self.id


class PaypalTxn(db.Model):
    txn_id = db.Column(db.String(24), primary_key=True, autoincrement=False)
    bot_version = db.Column(db.INTEGER)
    bot_chat_id = db.Column(db.ForeignKey('chat.id'), index=True)
    bot_email = db.Column(db.String(64), index=True)
    bot_service_id = db.Column(db.ForeignKey('service.id'), nullable=False, index=True)
    item_name = db.Column(db.String(128))
    quantity = db.Column(db.String(12))
    mc_gross = db.Column(db.String(12))
    test_ipn = db.Column(db.Boolean, default=False)
    residence_country = db.Column(db.String(12))
    mc_currency = db.Column(db.String(12))
    payment_date = db.Column(db.String(128))
    payer_name = db.Column(db.String(128))  # first_name last_name
    payer_email = db.Column(db.String(128))
    business = db.Column(db.String(128))
    comment = db.Column(db.String(128))
    date_in = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<PaypalTxn %r>' % self.txn_id


class CoingateTxn(db.Model):
    id = db.Column(db.String(64), primary_key=True, autoincrement=False)
    bot_version = db.Column(db.INTEGER)
    bot_chat_id = db.Column(db.ForeignKey('chat.id'), index=True)
    bot_email = db.Column(db.String(64), index=True)
    bot_service_id = db.Column(db.ForeignKey('service.id'), nullable=False, index=True)
    date_in = db.Column(db.DateTime, default=datetime.utcnow)
    comment = db.Column(db.String(128))

    order_id = db.Column(db.String(64))
    status = db.Column(db.String(12))
    price_amount = db.Column(db.String(12))
    price_currency = db.Column(db.String(12))
    receive_currency = db.Column(db.String(12))
    receive_amount = db.Column(db.String(12))
    pay_amount = db.Column(db.String(12))
    pay_currency = db.Column(db.String(12))
    created_at = db.Column(db.String(25))

    def __repr__(self):
        return '<CoingateTxn %r>' % self.invoice_id


class YandexMoneyTxn(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    bot_version = db.Column(db.INTEGER)
    bot_chat_id = db.Column(db.ForeignKey('chat.id'), index=True)
    bot_email = db.Column(db.String(64), index=True)
    bot_service_id = db.Column(db.ForeignKey('service.id'), nullable=False, index=True)
    date_in = db.Column(db.DateTime, default=datetime.utcnow)
    comment = db.Column(db.String(128))

    withdraw_amount = db.Column(db.String(24))
    amount = db.Column(db.String(24))
    notification_type = db.Column(db.String(24))
    unaccepted = db.Column(db.String(24))
    datetime = db.Column(db.String(24))
    operation_id = db.Column(db.String(64))
    operation_label = db.Column(db.String(64))
    codepro = db.Column(db.String(12))

    def __repr__(self):
        return '<YandexMoneyTxn %r>' % self.id


class ActionAlert(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    action = db.Column(db.String(128))
    date_in = db.Column(db.DateTime, default=datetime.utcnow)
    status = db.Column(db.String(128))
    chat_id = db.Column(db.ForeignKey('chat.id'), index=True)

    def __repr__(self):
        return '<ActionAlert %r>' % self.invoice_id


class ReferralAccount(db.Model):
    id = db.Column(db.ForeignKey('chat.id'), primary_key=True)
    balance = db.Column(db.INTEGER, nullable=False, default=0)
    wallet = db.Column(db.String(18))
    utm = db.Column(db.String(24), unique=True, index=True)
    cost = db.Column(db.INTEGER, nullable=False, default=90)

    chat = db.relationship('Chat', backref=db.backref('referral_account', lazy='dynamic'))

    def __repr__(self):
        return '<Referral Account %r>' % self.id


class ReferralTransactions(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    type = db.Column(db.String(6))  # credit / debit
    account_id = db.Column(db.ForeignKey('referral_account.id'), index=True, nullable=False)
    amount = db.Column(db.INTEGER, nullable=False, default=0)
    date_in = db.Column(db.DateTime, default=datetime.utcnow)
    trx_provide = db.Column(db.String(24))
    trx_id = db.Column(db.String(24))

    account = db.relationship('ReferralAccount', backref=db.backref('referral_transactions', lazy='dynamic'))

    def __repr__(self):
        return '<Referral Transaction %r>' % self.id


def after_insert_payment(mapper, connection, payment_tnx):
    '''
    Триггер для создание записи транзации в реферальной системе
    '''
    # считаем тех кто заплатил через бота
    if payment_tnx.bot_chat_id is None:
        return
    session = object_session(payment_tnx)
    if getattr(payment_tnx, 'id', None):
        tnx_id = payment_tnx.id
        ya_tnx_id = 0 if isinstance(payment_tnx.id, str) else tnx_id
    else:
        tnx_id = payment_tnx.txn_id
        ya_tnx_id = 0
    # считаем только пользователей которые платят впервые
    q1 = session.query(YandexMoneyTxn.bot_chat_id).filter(
        YandexMoneyTxn.bot_chat_id == payment_tnx.bot_chat_id).filter(
            ~(YandexMoneyTxn.id == ya_tnx_id)
        )
    q2 = session.query(PaypalTxn.bot_chat_id).filter(
        PaypalTxn.bot_chat_id == payment_tnx.bot_chat_id).filter(
            ~(PaypalTxn.txn_id == str(tnx_id))
        )
    q3 = session.query(CoingateTxn.bot_chat_id).filter(
        CoingateTxn.bot_chat_id == str(payment_tnx.bot_chat_id)).filter(
            ~(CoingateTxn.id == str(tnx_id))
        )
    q = q1.union(q2).union(q3)
    if q.count():
        return
    # определяем utm плательщика
    query = session.query(ReferralAccount).join(Chat, Chat.utm == ReferralAccount.utm)
    query = query.filter(Chat.id == payment_tnx.bot_chat_id)
    ref_account = query.scalar()
    if ref_account is None:
        return
    # транзакцию
    session.add(ReferralTransactions(
        type='credit',
        account_id=ref_account.id,
        amount=ref_account.cost,
        trx_provide=payment_tnx.__table__.name,
        trx_id=getattr(payment_tnx, 'id', None) or getattr(payment_tnx, 'txn_id', None)
    ))


db.event.listen(Chat, 'after_insert', Chat.after_insert)

db.event.listen(YandexMoneyTxn, 'after_insert', after_insert_payment)

db.event.listen(CoingateTxn, 'after_insert', after_insert_payment)

db.event.listen(PaypalTxn, 'after_insert', after_insert_payment)
