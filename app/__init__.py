from flask import request, views


class BaseView(views.View):

    def dispatch_request(self, *args, **kwargs):
        method_name = request.method.lower()
        method = getattr(self, method_name)
        return method(*args, **kwargs)
