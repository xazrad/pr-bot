import base64
import os
import urllib.parse as urlparse
from urllib.parse import urlencode

import requests
from flask import Blueprint, abort, current_app, redirect, request, render_template, url_for

from app import BaseView
from app.extensions import db
from app.models import Service
from celery_app import tasks


payment_bp = Blueprint('payment', __name__,
                       template_folder='templates',
                       static_folder='static')


class ProcessingView(BaseView):
    methods = ['GET']

    def _is_service_exists(self, custom):
        try:
            version, chat_id, service_id, = base64.urlsafe_b64decode(custom).decode().split(',')
        except:
            return False
        if version == '0':
            filter_by = {'chat_id': chat_id}
        else:
            filter_by = {'email_order': chat_id}
        query = db.session.query(Service).filter_by(id=service_id, date_out=None, **filter_by)
        if not db.session.query(query.exists()).scalar():
            return False
        return True

    def _get_redirect_url(self, custom):
        raise NotImplemented()

    def get(self):
        custom = request.args.get('q', '')
        if not self._is_service_exists(custom):
            return render_template('invalid_voice.html')
        return redirect(self._get_redirect_url(custom))


class ProcessingPaypalView(ProcessingView):

    def _get_redirect_url(self, custom):
        if os.environ.get('SETTINGS') == 'config.Production':
            # 249 рублей
            url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=MMWWJJJBN9MX2'
            # 65 рублей
            # url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6PHVBF85U92FG'
        else:
            url = url_for('payment.PaypalFakeFormView')
        params = {'custom': custom}

        url_parts = list(urlparse.urlparse(url))
        query = dict(urlparse.parse_qsl(url_parts[4]))
        query.update(params)
        url_parts[4] = urlencode(query)
        return urlparse.urlunparse(url_parts)


class PaypalFakeFormView(BaseView):
    methods = ['GET']

    def get(self):
        '''
        Перевод на sandbox страницу оплаты
        https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/formbasics/#form-attributes--action-and-method
        :return:
        '''
        if os.environ.get('SETTINGS') == 'config.Production':
            abort(404)
        custom = request.args.get('custom')
        return render_template('fake_form.html', custom=custom)


class PaypalIPN(BaseView):
    '''
    Уведомление об оплате paypal
    https://developer.paypal.com/docs/classic/products/instant-payment-notification/
    '''
    methods = ['POST']
    sandbox = False

    def post(self):
        current_app.logger.warning('%s -- %s', request.path, dict(request.form))
        if request.form['txn_type'] == 'web_accept' \
                and request.form['payment_status'] == 'Completed':
            tasks.verify_paypal_payment.delay(dict(request.form), sandbox=self.sandbox)
        return '', 200


class PaypalIPNTest(PaypalIPN):
    sandbox = True


class ProcessingCryptoView(ProcessingView):

    def _get_redirect_url(self, custom):
        if os.environ.get('SETTINGS') == 'config.Production':
            url = current_app.config['COINGATE_ENV_PROD']
            secret = current_app.config['COINGATE_SECRET_PROD']
        else:
            url = current_app.config['COINGATE_ENV_TEST']
            secret = current_app.config['COINGATE_SECRET_TEST']
        url = '%s/%s' % (url, 'orders')

        schema = current_app.config['PREFERRED_URL_SCHEME']
        server_name = current_app.config['SERVER_HOST']
        callback_url = '{}://{}/p/crypto/ipn/'.format(schema, server_name)

        params = {
            'order_id': custom,
            'price_amount': 3.99,
            'price_currency': 'USD',
            'title': 'MTProxy Server Rent',
            'callback_url': callback_url,
            'receive_currency': 'DO_NOT_CONVERT'
        }

        headers = {
            "Authorization": "Token %s" % secret
        }

        r = requests.post(url, headers=headers, data=params)
        return r.json()['payment_url']


class CryptoIPN(BaseView):
    methods = ['POST']

    def post(self):
        ''' Уведомление об оплате и прочих событиях
        https://developer.coingate.com/docs/payment-callback
        :return:
        '''
        current_app.logger.warning('%s -- %s', request.path, dict(request.form))
        if request.form['status'] in ['confirming', 'paid']:
            tasks.verify_coingate_payment.delay(dict(request.form))
        return '', 200


class YandexMoneyView(ProcessingView):

    def _get_redirect_url(self, custom):
        '''
        https://money.yandex.ru/transfer?receiver=41001329192457&sum=100&targets=erlgl&origin=form&selectedPaymentType=AC&form-comment=erlgl&quickpay-form=shop
        curl -d "receiver=41001329192457" -d "quickpay-form=shop" -d "targets=server1" -d "formcomment=server2"
        -d "paymentType=AC" -d "sum=10" -d "short-dest=server2" -d "label=server0" https://money.yandex.ru/quickpay/confirm.xml -v
        :param custom:
        :return:
        '''
        data = {
            'receiver': '41001329192457',
            'quickpay-form': 'shop',
            'paymentType': 'AC',
            'targets': 'Продление аренды',
            'formcomment': 'Продление аренды',
            'sum': '249',
            'label': custom
        }
        r = requests.post('https://money.yandex.ru/quickpay/confirm.xml', data=data)
        url = r.history[-1].text.replace('Found. Redirecting to', '').strip()
        return url


class YandexMoneyIPN(BaseView):
    methods = ['POST']

    def post(self):
        ''' Уведомление об оплате и прочих событиях
         {'currency': ['643'], 'label': ['MCw0MjMzNTI5MzQsMTg0'], 'firstname': [''],
         'withdraw_amount': ['10.00'],
         'suite': [''], 'email': [''],
         'sha1_hash': ['9c5862027f64e1524a6f17ac7e4eefe3764531b8'],
         'notification_type': ['card-incoming'],
         'unaccepted': ['false'],
         'datetime': ['2019-03-05T20:39:43Z'],
         'operation_id': ['605133583478041012'],
         'zip': [''], 'building': [''], 'phone': [''], 'flat': [''],
         'amount': ['9.80'],
         'lastname': [''],
         'codepro': ['false'], 'city': [''], 'sender': [''], 'fathersname': [''],
         'operation_label': ['2410f235-0011-5000-9000-1686f41d84ba'], 'street': ['']}
        :return:
        '''
        current_app.logger.warning('%s -- %s', request.path, dict(request.form))
        tasks.vefify_yandex_payment.delay(dict(request.form))
        return '', 200


payment_bp.add_url_rule('/paypal/ipn/', view_func=PaypalIPN.as_view('PaypalIPN'))
payment_bp.add_url_rule('/paypal/ipn/test/', view_func=PaypalIPNTest.as_view('PaypalIPNTest'))
payment_bp.add_url_rule('/paypal/fake/', view_func=PaypalFakeFormView.as_view('PaypalFakeFormView'))
payment_bp.add_url_rule('/paypal/', view_func=ProcessingPaypalView.as_view('ProcessingPaypalView'))

payment_bp.add_url_rule('/yandex/', view_func=YandexMoneyView.as_view('YandexMoneyView'))
payment_bp.add_url_rule('/yandex/ipn/', view_func=YandexMoneyIPN.as_view('YandexMoneyIPN'))

payment_bp.add_url_rule('/crypto/', view_func=ProcessingCryptoView.as_view('ProcessingCryptoView'))
payment_bp.add_url_rule('/crypto/ipn/', view_func=CryptoIPN.as_view('CryptoIPN'))
