(function($) {
  
  "use strict";

/* 
   One Page Navigation & wow js
   ========================================================================== */
    //Initiat WOW JS
    new WOW().init();

/* 
   Page Loader
   ========================================================================== */
  $('#loader').fadeOut();

/* Send e-mail*/ 
	
	$("#sendemail1").click(function(){
	  sendEmail("#email1");
	});
	$("#sendemail2").click(function(){
	  sendEmail("#email2");
	});
  
	function sendEmail(id){	
		var email = $(id).val();
		$.ajax({
			type: "POST",
			url: "https://get-mtproxy.sgtel.win/v1/order/",
			dataType : "json",
			data: {
				email: email
			},
			success: respMsg
		})	 ;
		
	}

	function respMsg(data){
		if(data.ok == false) {
			switch (data.error_code){
				case 400:window.location.href = "invalid_email.html";
				break;
				case 402:window.location.href = "payment_required.html";
				break;
			}
		} else {
			window.location.href = "done.html";
		}
	
	}
  
}(jQuery));


