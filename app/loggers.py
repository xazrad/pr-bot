import logging
from logging.handlers import RotatingFileHandler


LOGGING = {
    'handlers': {
        'debug_handler': {
            'class': RotatingFileHandler,
            'file_log': True,
            'params': {'maxBytes': 100000, 'backupCount': 9},
            'level': logging.DEBUG,
            'format': '%(asctime)s - %(levelname)s - %(message)s'
        },
        'error_handler': {
            'class': RotatingFileHandler,
            'file_error': True,
            'params': {'maxBytes': 100000, 'backupCount': 9},
            'level': logging.ERROR,
            'format': '%(asctime)s\t%(levelname)s -- %(processName)s %(filename)s:%(lineno)s -- %(message)s'
        }
    }
}


class LoggingConfig(object):
    def __init__(self, file_log, file_error):
        self.file_log = str(file_log)
        self.file_error = str(file_error)

        self.handlers = []
        self._make_hadlers()

    def _make_hadlers(self):
        for key, handler_values in LOGGING['handlers'].items():
            cl = handler_values['class']
            params = handler_values['params']
            if handler_values.get('file_log'):
                params['filename'] = self.file_log
            if handler_values.get('file_error'):
                params['filename'] = self.file_error

            handler = cl(**params)
            handler.setFormatter(logging.Formatter(handler_values['format']))
            handler.setLevel(handler_values['level'])
            if handler_values.get('filter'):
                handler.addFilter(handler_values['filter'])

            self.handlers.append(handler)
